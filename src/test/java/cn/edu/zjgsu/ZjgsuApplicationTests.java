package cn.edu.zjgsu;

import cn.edu.zjgsu.dao.LearncommentMapper;
import cn.edu.zjgsu.model.Learncomment;
import cn.edu.zjgsu.model.LearncommentExample;
import cn.edu.zjgsu.model.UserCourseView;
import cn.edu.zjgsu.service.LearnCommentService;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.ResultPageBean;
import com.sun.xml.internal.xsom.impl.ListSimpleTypeImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZjgsuApplicationTests {

	@Autowired
	private LearncommentMapper learncommentMapper;
	@Autowired
	private UserInfoService userInfoService;


	@Test
	public void contextLoads() {
	}

	@Test
	public void getCommentTest()
	{
		System.out.println("============");
		LearncommentExample learncommentExample = new LearncommentExample();
		learncommentExample.or().andCourseIdEqualTo(44637);
		List<Learncomment> learncomments = learncommentMapper.selectByExampleWithBLOBs(learncommentExample);
		for (int i=0;i<learncomments.size();i++)
		{
			System.out.println("=========");
			System.out.println(learncomments.get(i).getCmtComment());
		}
	}

	@Test
	public void userFollowCourseTest()
	{
		Long time1 = System.currentTimeMillis();
		ResultPageBean resultPageBean =  userInfoService.getUserFollowCourseByPaging(1459472789L, 1, 10);
		//System.out.println("========");
		//System.out.println(System.currentTimeMillis()-time1);
		time1 = System.currentTimeMillis();
		for (int i=0;i<10;i++)
		resultPageBean =  userInfoService.getUserFollowCourseByPaging(1459472789L, 1, 10);
		//System.out.println("===========");
		//System.out.println(System.currentTimeMillis()-time1);
		List<UserCourseView> userCourseViews = (List<UserCourseView>) resultPageBean.getData();
	}

}
