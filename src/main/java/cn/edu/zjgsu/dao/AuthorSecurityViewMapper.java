package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.AuthorSecurityView;
import cn.edu.zjgsu.model.AuthorSecurityViewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface AuthorSecurityViewMapper {
    long countByExample(AuthorSecurityViewExample example);

    int deleteByExample(AuthorSecurityViewExample example);

    int insert(AuthorSecurityView record);

    int insertSelective(AuthorSecurityView record);

    List<AuthorSecurityView> selectByExample(AuthorSecurityViewExample example);

    int updateByExampleSelective(@Param("record") AuthorSecurityView record, @Param("example") AuthorSecurityViewExample example);

    int updateByExample(@Param("record") AuthorSecurityView record, @Param("example") AuthorSecurityViewExample example);
}