package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Userfollowcourse;
import cn.edu.zjgsu.model.UserfollowcourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserfollowcourseMapper {
    long countByExample(UserfollowcourseExample example);

    int deleteByExample(UserfollowcourseExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Userfollowcourse record);

    int insertSelective(Userfollowcourse record);

    List<Userfollowcourse> selectByExample(UserfollowcourseExample example);

    Userfollowcourse selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Userfollowcourse record, @Param("example") UserfollowcourseExample example);

    int updateByExample(@Param("record") Userfollowcourse record, @Param("example") UserfollowcourseExample example);

    int updateByPrimaryKeySelective(Userfollowcourse record);

    int updateByPrimaryKey(Userfollowcourse record);
}