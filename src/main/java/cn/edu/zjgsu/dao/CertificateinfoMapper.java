package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Certificateinfo;
import cn.edu.zjgsu.model.CertificateinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface CertificateinfoMapper {
    long countByExample(CertificateinfoExample example);

    int deleteByExample(CertificateinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Certificateinfo record);

    int insertSelective(Certificateinfo record);

    List<Certificateinfo> selectByExample(CertificateinfoExample example);

    Certificateinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Certificateinfo record, @Param("example") CertificateinfoExample example);

    int updateByExample(@Param("record") Certificateinfo record, @Param("example") CertificateinfoExample example);

    int updateByPrimaryKeySelective(Certificateinfo record);

    int updateByPrimaryKey(Certificateinfo record);
}