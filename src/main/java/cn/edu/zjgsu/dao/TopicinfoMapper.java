package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Topicinfo;
import cn.edu.zjgsu.model.TopicinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface TopicinfoMapper {
    long countByExample(TopicinfoExample example);

    int deleteByExample(TopicinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Topicinfo record);

    int insertSelective(Topicinfo record);

    List<Topicinfo> selectByExample(TopicinfoExample example);

    Topicinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Topicinfo record, @Param("example") TopicinfoExample example);

    int updateByExample(@Param("record") Topicinfo record, @Param("example") TopicinfoExample example);

    int updateByPrimaryKeySelective(Topicinfo record);

    int updateByPrimaryKey(Topicinfo record);

    @Select("select count(*) from (select count(*) as count from topicinfo group by author_name) as u " +
            "where u.count > #{min} and u.count >#{max}")
    int getCountOfNumberSeeTopic(@Param("min")Integer min,@Param("max")Integer max);
}