package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Coursedetail;
import cn.edu.zjgsu.model.CoursedetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface CoursedetailMapper {
    long countByExample(CoursedetailExample example);

    int deleteByExample(CoursedetailExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Coursedetail record);

    int insertSelective(Coursedetail record);

    List<Coursedetail> selectByExample(CoursedetailExample example);

    @Select("select distinct class_name from coursedetail")
    List<String> selectDifferentClassName();

    @Select("select count(*) from coursedetail where class_name = #{className}")
    Integer countByClassName(String className);

    Coursedetail selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Coursedetail record, @Param("example") CoursedetailExample example);

    int updateByExample(@Param("record") Coursedetail record, @Param("example") CoursedetailExample example);

    int updateByPrimaryKeySelective(Coursedetail record);

    int updateByPrimaryKey(Coursedetail record);
}