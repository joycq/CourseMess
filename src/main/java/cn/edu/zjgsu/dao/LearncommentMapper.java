package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Learncomment;
import cn.edu.zjgsu.model.LearncommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LearncommentMapper {
    long countByExample(LearncommentExample example);

    int deleteByExample(LearncommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Learncomment record);

    int insertSelective(Learncomment record);

    List<Learncomment> selectByExampleWithBLOBs(LearncommentExample example);

    List<Learncomment> selectByExample(LearncommentExample example);

    Learncomment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Learncomment record, @Param("example") LearncommentExample example);

    int updateByExampleWithBLOBs(@Param("record") Learncomment record, @Param("example") LearncommentExample example);

    int updateByExample(@Param("record") Learncomment record, @Param("example") LearncommentExample example);

    int updateByPrimaryKeySelective(Learncomment record);

    int updateByPrimaryKeyWithBLOBs(Learncomment record);

    int updateByPrimaryKey(Learncomment record);

}