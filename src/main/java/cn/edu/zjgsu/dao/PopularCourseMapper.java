package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.PopularCourse;
import cn.edu.zjgsu.model.PopularCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PopularCourseMapper {
    long countByExample(PopularCourseExample example);

    int deleteByExample(PopularCourseExample example);

    int deleteByPrimaryKey(Long popularCourseId);

    int insert(PopularCourse record);

    int insertSelective(PopularCourse record);

    List<PopularCourse> selectByExample(PopularCourseExample example);

    PopularCourse selectByPrimaryKey(Long popularCourseId);

    int updateByExampleSelective(@Param("record") PopularCourse record, @Param("example") PopularCourseExample example);

    int updateByExample(@Param("record") PopularCourse record, @Param("example") PopularCourseExample example);

    int updateByPrimaryKeySelective(PopularCourse record);

    int updateByPrimaryKey(PopularCourse record);
}