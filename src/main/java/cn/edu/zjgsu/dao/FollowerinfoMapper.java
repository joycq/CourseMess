package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Followerinfo;
import cn.edu.zjgsu.model.FollowerinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FollowerinfoMapper {
    long countByExample(FollowerinfoExample example);

    int deleteByExample(FollowerinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Followerinfo record);

    int insertSelective(Followerinfo record);

    List<Followerinfo> selectByExample(FollowerinfoExample example);

    Followerinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Followerinfo record, @Param("example") FollowerinfoExample example);

    int updateByExample(@Param("record") Followerinfo record, @Param("example") FollowerinfoExample example);

    int updateByPrimaryKeySelective(Followerinfo record);

    int updateByPrimaryKey(Followerinfo record);
}