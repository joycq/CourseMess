package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Userstudycourse;
import cn.edu.zjgsu.model.UserstudycourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserstudycourseMapper {
    long countByExample(UserstudycourseExample example);

    int deleteByExample(UserstudycourseExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Userstudycourse record);

    int insertSelective(Userstudycourse record);

    List<Userstudycourse> selectByExample(UserstudycourseExample example);

    Userstudycourse selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Userstudycourse record, @Param("example") UserstudycourseExample example);

    int updateByExample(@Param("record") Userstudycourse record, @Param("example") UserstudycourseExample example);

    int updateByPrimaryKeySelective(Userstudycourse record);

    int updateByPrimaryKey(Userstudycourse record);
}