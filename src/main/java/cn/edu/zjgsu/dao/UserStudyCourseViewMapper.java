package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.UserStudyCourseView;
import cn.edu.zjgsu.model.UserStudyCourseViewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserStudyCourseViewMapper {
    long countByExample(UserStudyCourseViewExample example);

    int deleteByExample(UserStudyCourseViewExample example);

    int insert(UserStudyCourseView record);

    int insertSelective(UserStudyCourseView record);

    List<UserStudyCourseView> selectByExample(UserStudyCourseViewExample example);

    int updateByExampleSelective(@Param("record") UserStudyCourseView record, @Param("example") UserStudyCourseViewExample example);

    int updateByExample(@Param("record") UserStudyCourseView record, @Param("example") UserStudyCourseViewExample example);
}