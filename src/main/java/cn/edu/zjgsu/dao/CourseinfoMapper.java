package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Courseinfo;
import cn.edu.zjgsu.model.CourseinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CourseinfoMapper {
    long countByExample(CourseinfoExample example);

    int deleteByExample(CourseinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Courseinfo record);

    int insertSelective(Courseinfo record);

    List<Courseinfo> selectByExample(CourseinfoExample example);

    Courseinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Courseinfo record, @Param("example") CourseinfoExample example);

    int updateByExample(@Param("record") Courseinfo record, @Param("example") CourseinfoExample example);

    int updateByPrimaryKeySelective(Courseinfo record);

    int updateByPrimaryKey(Courseinfo record);
}