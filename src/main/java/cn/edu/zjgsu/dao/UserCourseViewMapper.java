package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.UserCourseView;
import cn.edu.zjgsu.model.UserCourseViewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserCourseViewMapper {
    long countByExample(UserCourseViewExample example);

    int deleteByExample(UserCourseViewExample example);

    int insert(UserCourseView record);

    int insertSelective(UserCourseView record);

    List<UserCourseView> selectByExample(UserCourseViewExample example);

    int updateByExampleSelective(@Param("record") UserCourseView record, @Param("example") UserCourseViewExample example);

    int updateByExample(@Param("record") UserCourseView record, @Param("example") UserCourseViewExample example);
}