package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.CourseView;
import cn.edu.zjgsu.model.CourseViewExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CourseViewMapper {
    long countByExample(CourseViewExample example);

    int deleteByExample(CourseViewExample example);

    int insert(CourseView record);

    int insertSelective(CourseView record);

    List<CourseView> selectByExample(CourseViewExample example);

    int updateByExampleSelective(@Param("record") CourseView record, @Param("example") CourseViewExample example);

    int updateByExample(@Param("record") CourseView record, @Param("example") CourseViewExample example);
}