package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Recommend;
import cn.edu.zjgsu.model.RecommendExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface RecommendMapper {
    long countByExample(RecommendExample example);

    int deleteByExample(RecommendExample example);

    int deleteByPrimaryKey(Long recommmend);

    int insert(Recommend record);

    int insertSelective(Recommend record);

    List<Recommend> selectByExample(RecommendExample example);

    Recommend selectByPrimaryKey(Long recommmend);

    int updateByExampleSelective(@Param("record") Recommend record, @Param("example") RecommendExample example);

    int updateByExample(@Param("record") Recommend record, @Param("example") RecommendExample example);

    int updateByPrimaryKeySelective(Recommend record);

    int updateByPrimaryKey(Recommend record);

    @Select(
            "select course_name_cn,MAX(weight) FROM recommend WHERE author_name = #{authorName} " +
                    "GROUP BY course_name_cn ORDER BY MAX(weight) DESC LIMIT #{pageSize};")
    List<String> selectRecommendCourseByAuthorName(@Param("authorName") String authorName,@Param("pageSize") Integer pageSize);
}