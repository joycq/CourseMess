package cn.edu.zjgsu.dao;

import cn.edu.zjgsu.model.Lectureinfo;
import cn.edu.zjgsu.model.LectureinfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface LectureinfoMapper {
    long countByExample(LectureinfoExample example);

    int deleteByExample(LectureinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Lectureinfo record);

    int insertSelective(Lectureinfo record);

    List<Lectureinfo> selectByExample(LectureinfoExample example);

    Lectureinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Lectureinfo record, @Param("example") LectureinfoExample example);

    int updateByExample(@Param("record") Lectureinfo record, @Param("example") LectureinfoExample example);

    int updateByPrimaryKeySelective(Lectureinfo record);

    int updateByPrimaryKey(Lectureinfo record);

    @Select("select COUNT(*) FROM (SELECT author_name,COUNT(*) as count FROM lectureinfo GROUP BY author_name) as u where u.count >#{min} and u.count<#{max}")
    int getCountOfNumberSeeLecture(@Param("min") Integer min, @Param("max") Integer max);
}