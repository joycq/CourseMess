package cn.edu.zjgsu.templates;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by wqmei on 2017/10/2.
 */
@Scope("prototype")
@Component
public class ResultBean<T> implements Serializable
{
    private static final long seriaVersionUID = 1L;

    public static final int NO_LOGIN = -1;

    public static final int SUCCESS = 1;

    public static final int FAIL = 0;

    public static final int NO_PERMISSION = 2;

    private String msg = "success";

    private int code = SUCCESS;

    private T data;

    public void fail()
    {
        this.code = FAIL;
    }

    public void noAuthorId()
    {
        this.code = FAIL;
        this.msg = "no authorId in session when running";
    }

    public void wrongParameter()
    {
        this.code = HttpStatus.WRONG_PARAMETER;
    }


    public ResultBean()
    {
        super();
    }

    public ResultBean(T data)
    {
        super();
        this.data = data;
    }

    public ResultBean(Throwable e)
    {
        super();
        this.msg = e.toString();
        this.code = FAIL;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }

}
