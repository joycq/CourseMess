package cn.edu.zjgsu.templates;

/**
 * @author: wqmei
 * DateTime: 2017/10/29 12:49
 * Description: 返回值中的返回码(code)
 */
public final class HttpStatus
{
    /**
     * 成功
     */
    public static final int SUCCESS = 200;

    /**
     * 失败
     */
    public static final int FAIL = 500;

    /**
     * 参数不符合要求
     */
    public static final int WRONG_PARAMETER = 400;

    /**
     * 权限不通过
     */
    public static final int NO_PERMISSION = 401;

}
