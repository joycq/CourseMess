package cn.edu.zjgsu.templates;


import org.omg.IOP.TAG_ORB_TYPE;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wqmei on 2017/10/2.
 */
@Component
@Scope("prototype")
public class ResultPageBean<T> implements Serializable
{
    private static final long seriaVersionUID = 1L;

    public static final int NO_LOGIN = -1;

    public static final int SUCCESS = 1;

    public static final int FAIL = 0;

    public static final int NO_PERMISSION = 2;

    private String msg = "success";

    private int code = SUCCESS;

    private List<T> data;

    private int totalCount;

    public void fail()
    {
        this.code = FAIL;
    }

    public void noAuthorId()
    {
        this.code = FAIL;
        this.msg = "no authorId in session when running";
    }

    public void illegalPageCountOrPageSize()
    {
        this.code = FAIL;
        this.msg = "illegal page";
    }

    public ResultPageBean()
    {
        super();
    }

    public ResultPageBean(List<T> data)
    {
        super();
        this.data = data;
    }

    public ResultPageBean(Throwable e)
    {
        super();
        this.msg = e.toString();
        this.code = FAIL;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public List<T> getData()
    {
        return data;
    }

    public void setData(List<T> data)
    {
        this.data = data;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public void setTotalCount(Long totalCount)
    {
        this.totalCount = (int) (long) totalCount;
    }
}
