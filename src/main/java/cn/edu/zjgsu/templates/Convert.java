package cn.edu.zjgsu.templates;

/**
 * @author: wqmei
 * DateTime: 2017/11/8 16:05
 * Description:
 */
public interface Convert<T,S>
{
    S convert();

    T reconvert(S s);
}
