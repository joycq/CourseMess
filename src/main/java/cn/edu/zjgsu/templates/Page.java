package cn.edu.zjgsu.templates;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 9:31
 * Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T>
{
    private List<T> data;

    private Long totalCount;
}
