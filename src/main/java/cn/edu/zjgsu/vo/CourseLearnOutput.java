package cn.edu.zjgsu.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 13:42
 * Description:
 */
@Data
@Builder
public class CourseLearnOutput
{
    private Long totalCount;
    private Long validateCount;
    private Long notValidateCount;
}
