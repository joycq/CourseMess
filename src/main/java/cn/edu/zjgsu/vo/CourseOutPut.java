package cn.edu.zjgsu.vo;

import cn.edu.zjgsu.model.CourseView;
import cn.edu.zjgsu.templates.Convert;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * @author: wqmei
 * DateTime: 2017/11/20 15:24
 * Description:
 */
@Data
public class CourseOutPut implements Convert<CourseOutPut,CourseView>
{

    /**
     * 总数
     */
    private Long totalCount;
    /**
     * 通过验证数
     */
    private Long validateCount;
    /**
     * 未通过验证数
     */
    private Long notValidateCount;

    private String className;

    private Integer courseId;

    private String courseNameCn;

    private String courseNameEn;

    private String courseUrl;

    private String star;

    private String score;

    private String personFollow;

    private String startTime;

    private String durationTime;

    private String courseLevel;

    private String knowledgeV;

    private String teacherV;

    private String interestV;

    private String designV;

    private String commentTag;

    private String courseScorePeople;

    private Integer remarkNum;

    private Integer noteNum;

    private Integer talkNum;

    private String school;

    private String platform;

    private String lang;

    private String teacher;



    @Override
    public CourseView convert()
    {
        return null;
    }

    @Override
    public CourseOutPut reconvert(CourseView courseView)
    {
        BeanUtils.copyProperties(courseView,this);
        return this;
    }
}
