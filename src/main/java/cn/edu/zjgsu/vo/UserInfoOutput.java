package cn.edu.zjgsu.vo;

import lombok.Data;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 9:24
 * Description:
 */
@Data
public class UserInfoOutput
{

    /**
     * 用户id
     */
    private Long authorId;

    /**
     * 用户昵称
     */
    private String authorName;

    /**
     * 用户课程数
     */
    private Long courseNumber;

    /**
     * 用户演讲数
     */
    private Long lectureNumber;

    /**
     * 用户专题数
     */
    private Long topicNumber;
}
