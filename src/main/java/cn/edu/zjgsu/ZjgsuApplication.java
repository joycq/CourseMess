package cn.edu.zjgsu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("cn.edu.zjgsu.dao")
@EnableCaching
public class ZjgsuApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(ZjgsuApplication.class);
		springApplication.run(args);
	}

}
