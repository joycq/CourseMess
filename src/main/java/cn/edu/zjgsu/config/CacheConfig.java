package cn.edu.zjgsu.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig
{

    /**
     * ehcache config
     *
     * @return
     */
    @Bean
    CacheManager cacheManager()
    {
        CacheManager cacheManager = new EhCacheCacheManager();
        return cacheManager;
    }

}
