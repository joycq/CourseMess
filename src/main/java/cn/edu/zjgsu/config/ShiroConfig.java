package cn.edu.zjgsu.config;

import cn.edu.zjgsu.security.Realm;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * User: wqmei
 * Date: 2017/10/17
 * Time: 18:04
 * Description: shiro配置类
 */
@Configuration
public class ShiroConfig
{
    private static final Logger logger = LoggerFactory.getLogger(ShiroConfig.class);

    @Bean(name="shiroFilter")
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager)
    {
        logger.info("注入shiro过滤器 ",ShiroFilterFactoryBean.class);

        //以下相当于shiro配置的xml文件的java写法

        //注入ShiroFilterFactoryBean
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        //以下为注入bean的初始化属性,类似<propery>

        //shiro核心
        shiroFilterFactoryBean.setSecurityManager(securityManager);


        //设置登录url
        shiroFilterFactoryBean.setLoginUrl("/user/login.html");
/*        //登录成功后跳转的url
        shiroFilterFactoryBean.setSuccessUrl("/index.html");
        //无权限的跳转页面
        shiroFilterFactoryBean.setUnauthorizedUrl("/403.html");
*/

        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();

        /**
         *相当于xml设置某些权限可以访问的页面,可以将有权限要求的页面进行保护
         * anno代表任何人可以访问 authc需要登录才能访问
         */
        filterChainDefinitionMap.put("/login.html", "anon");
        filterChainDefinitionMap.put("/admin/*", "authc");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        return shiroFilterFactoryBean;
    }


    /**
     * 等于xml 注入DefaultSecurityManager,属性为Realm这个bean,class是相应类
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager securityManager(Realm realm)
    {
        logger.info("注入SecurityManager ", Realm.class);
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(realm);
        //securityManager.setCacheManager(ehCacheManager());
        return securityManager;
    }

    /*@Bean
    public EhCacheManager ehCacheManager()
    {
        EhCacheManager ehCacheManager = new EhCacheManager();
        ehCacheManager.setCacheManagerConfigFile("classpath:ehcache-shiro.xml");
        return ehCacheManager;
    }*/


    /**
     * 生命周期处理器
     * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor()
    {
        return new LifecycleBeanPostProcessor();
    }


    /**
     * 开启shiro注解
     * @return
     */
    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator()
    {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager)
    {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }





}
