package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.PopularCourse;
import cn.edu.zjgsu.model.Recommend;
import cn.edu.zjgsu.templates.Page;

import java.util.List;

/**
 * @author: wqmei
 * DateTime: 2017/11/23 16:52
 * Description:
 */
public interface RecommendService
{
    Page<Recommend> getRecommendByAuthorName(String authorName, Integer pageNumber, Integer pageSize);

    Page<PopularCourse> getPopularCoursePage(Integer pageNumber, Integer pageSize);

    List<String> getRecommendCourseNameByAuthorName(String authorName, Integer pageSize);
}
