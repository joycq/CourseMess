package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.Topicinfo;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;

import java.util.List;

public interface TopicService
{
    Page<Topicinfo> getTopicsByPaging(Long authorId, Integer pageCount, Integer pageSize);

    ResultBean<Integer> getTopicCount(Long authorId);

    ResultBean<Integer> getTopicNumberOfPeople(List<Integer> rangeList);
}
