package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.AuthorMapper;
import cn.edu.zjgsu.dao.RoleMapper;
import cn.edu.zjgsu.model.Author;
import cn.edu.zjgsu.model.AuthorExample;
import cn.edu.zjgsu.model.Role;
import cn.edu.zjgsu.model.RoleExample;
import cn.edu.zjgsu.service.AuthorService;
import cn.edu.zjgsu.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

/**
 * Created by wqmei on 2017/10/2.
 */
@Service
public class AuthorServiceImpl implements AuthorService
{
    @Autowired
    private AuthorMapper authorMapper;
    @Autowired
    private RoleMapper roleMapper;

    private AuthorExample authorExample;

    @Override
    public Boolean login(Author author)
    {
        authorExample = new AuthorExample();
        if (Objects.equals(checkAuthor(author), Boolean.FALSE))
        {
            return Boolean.FALSE;
        }
        authorExample.or().andLoginNameEqualTo(author.getLoginName()).andLoginPwdEqualTo(author.getLoginPwd());

        if (authorMapper.selectByExample(authorExample).size() > 0)
        {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public Long getAuthorId(Author author)
    {
        authorExample = new AuthorExample();
        authorExample.or().andLoginNameEqualTo((author.getLoginName())).andLoginPwdEqualTo(author.getLoginPwd());
        List<Author> authorList = authorMapper.selectByExample(authorExample);
        Author author1 = authorList.get(0);
        return author1.getAuthorId();
    }

    @Override
    public String getRoleByLoginName(String loginName)
    {
        RoleExample example = new RoleExample();
        example.or().andLoginNameEqualTo(loginName);
        List<Role> roleList = roleMapper.selectByExample(example);
        RoleExample roleExample = new RoleExample();
        roleExample.or().andLoginNameEqualTo("123456");
        System.out.println("结果为: "+roleMapper.countByExample(roleExample)+" 查询全部为： "+roleMapper.countByExample(new RoleExample()));
        if (roleList.size() == 0)
        {
            return null;
        }else
        return roleList.get(0).getRoleName();
    }

    private String encrypPasswordByMD5(String password) throws NoSuchAlgorithmException
    {
        //加密过程,等后续在使用
        /*MessageDigest sha1 = MessageDigest.getInstance("SHA");
        sha1.update((password+ Constant.ENCRYP_SALT).getBytes());
        return new String(sha1.digest());*/
        return password;
    }

    private Boolean checkAuthor(Author author)
    {
        if ((checkStrIsEmpty(author.getLoginName()) && checkStrIsEmpty(author.getLoginPwd()) == false))
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    private Boolean checkStrIsEmpty(String str)
    {
        return StringUtils.isEmpty(str);
    }


}
