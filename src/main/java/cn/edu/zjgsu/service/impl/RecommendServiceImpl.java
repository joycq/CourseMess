package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.PopularCourseMapper;
import cn.edu.zjgsu.dao.RecommendMapper;
import cn.edu.zjgsu.model.PopularCourse;
import cn.edu.zjgsu.model.PopularCourseExample;
import cn.edu.zjgsu.model.Recommend;
import cn.edu.zjgsu.model.RecommendExample;
import cn.edu.zjgsu.service.RecommendService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.util.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: wqmei
 * DateTime: 2017/11/23 16:53
 * Description:
 */
@Service
@Slf4j
public class RecommendServiceImpl implements RecommendService
{
    @Autowired
    RecommendMapper recommendMapper;
    @Autowired
    PopularCourseMapper popularCourseMapper;

    @Override
    public Page<Recommend> getRecommendByAuthorName(String authorName, Integer pageNumber, Integer pageSize)
    {
        Page<Recommend> page = new Page<>();
        RecommendExample example = new RecommendExample();
        if (PageUtil.validPage(pageNumber, pageSize))
        {
            example.setOffset(PageUtil.getOffset(pageNumber,pageSize));
            example.setLimit(pageSize);
        }
        example.setOrderByClause("weight DESC");
        example.or().andAuthorNameEqualTo(authorName);
        page.setData(recommendMapper.selectByExample(example));
        return page;
    }

    @Override
    public Page<PopularCourse> getPopularCoursePage(Integer pageNumber, Integer pageSize)
    {
        Page<PopularCourse> page = new Page<>();
        PopularCourseExample example = new PopularCourseExample();
        if(PageUtil.validPage(pageNumber,pageSize))
        {
            example.setOffset(PageUtil.getOffset(pageNumber,pageSize));
            example.setLimit(pageSize);
        }
        example.setOrderByClause("number DESC");
        page.setData(popularCourseMapper.selectByExample(example));
        page.setTotalCount(popularCourseMapper.countByExample(example));
        return page;
    }

    @Override
    public List<String> getRecommendCourseNameByAuthorName(String authorName, Integer pageSize)
    {
        /*
        sql
        select course_name_cn,MAX(weight) FROM recommend WHERE author_name = "诗与剑入醉" GROUP BY course_name_cn ORDER BY MAX(weight) DESC LIMIT 2;
         */
        if (pageSize == null)
        {
            pageSize = 2;
        }
        return recommendMapper.selectRecommendCourseByAuthorName(authorName,pageSize);
    }
}
