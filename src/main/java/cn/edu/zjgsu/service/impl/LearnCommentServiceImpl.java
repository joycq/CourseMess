package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.LearncommentMapper;
import cn.edu.zjgsu.model.Learncomment;
import cn.edu.zjgsu.model.LearncommentExample;
import cn.edu.zjgsu.service.LearnCommentService;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.CheckUtil;
import cn.edu.zjgsu.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LearnCommentServiceImpl implements LearnCommentService
{
    @Autowired
    private LearncommentMapper learncommentMapper;

    private LearncommentExample learncommentExample;

    private ResultPageBean resultPageBean;

    @Override
    public ResultPageBean<Learncomment> getLearnCommentByCourseIdPaging(Integer courseId,Integer pageCount,Integer pageSize)
    {
        learncommentExample = new LearncommentExample();
        resultPageBean = new ResultPageBean<Learncomment>();

        if (courseId == null)
        {
            resultPageBean.noAuthorId();
            return resultPageBean;
        }
        if (CheckUtil.checkPage(pageCount, pageSize))
        {
            learncommentExample.setOffset(CommonUtil.getOffset(pageCount,pageSize));
            learncommentExample.setLimit(pageSize);
            List<Learncomment> learncomments = learncommentMapper.selectByExampleWithBLOBs(learncommentExample);
            resultPageBean.setData(learncomments);
            resultPageBean.setTotalCount((int)learncommentMapper.countByExample(learncommentExample));
            return resultPageBean;
        }
        resultPageBean.illegalPageCountOrPageSize();
        return resultPageBean;
    }
}
