package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.CertificateinfoMapper;
import cn.edu.zjgsu.dao.CourseViewMapper;
import cn.edu.zjgsu.dao.CoursedetailMapper;
import cn.edu.zjgsu.dao.UserCourseViewMapper;
import cn.edu.zjgsu.model.*;
import cn.edu.zjgsu.service.CourseInfoService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.CheckUtil;
import cn.edu.zjgsu.util.CommonUtil;
import cn.edu.zjgsu.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.View;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CourseInfoServiceImpl implements CourseInfoService
{
    @Autowired
    private CourseViewMapper courseViewMapper;
    @Autowired
    private CoursedetailMapper coursedetailMapper;
    @Autowired
    UserCourseViewMapper userCourseViewMapper;
    @Autowired
    CertificateinfoMapper certificateinfoMapper;


    private CourseViewExample courseViewExample;


    private ResultPageBean resultPageBean;


    private ResultBean resultBean;

    @Override
    public Page<CourseView> getCourseInfoByPaging(String keyword, Integer pageCount, Integer pageSize)
    {
        courseViewExample = new CourseViewExample();
        Page<CourseView> page = new Page();
        if (CheckUtil.checkPage(pageCount, pageSize))
        {
            courseViewExample.setOffset(CommonUtil.getOffset(pageCount,pageSize));
            courseViewExample.setLimit(pageSize);
            if (StringUtil.isNotEmpty(keyword))
            {
                courseViewExample.or().andCourseNameCnLike("%"+keyword+"%");
                courseViewExample.or().andCourseNameEnLike("%" + keyword + "%");
            }
            List<CourseView> courseViews = courseViewMapper.selectByExample(courseViewExample);
            page.setData(courseViews);
            page.setTotalCount(courseViewMapper.countByExample(courseViewExample));
            return page;
        }
        throw new RuntimeException("查询课程信息出错");
    }

    @Override
    public ResultBean<Map<String, Integer>> getDifferentClassNumber()
    {
        resultBean = new ResultBean<Map<String,Integer>>();

        Map<String, Integer> map = new HashMap<>();
        List<String> classNames =  coursedetailMapper.selectDifferentClassName();
        for (int i =0,length = classNames.size();i<length;i++)
        {
            Integer num = coursedetailMapper.countByClassName(classNames.get(i));
            map.put(classNames.get(i), num);
        }
        resultBean.setData(map);
        return resultBean;
    }

    @Override
    public Long getLearnerNumberByCourseId(Integer courseId)
    {
        UserCourseViewExample example = new UserCourseViewExample();
        example.or().andCourseIdEqualTo(courseId);
        return userCourseViewMapper.countByExample(example);
    }

    @Override
    public Long getLearnerNumberByCourseUrl(String courseUrl)
    {
        UserCourseViewExample example = new UserCourseViewExample();
        example.or().andCourseUrlEqualTo(courseUrl);
        return userCourseViewMapper.countByExample(example);
    }

    @Override
    public Long getLearnerNumberByCourseNameCn(String courseNameCn)
    {
        CertificateinfoExample example = new CertificateinfoExample();
        example.or().andCourseNameCnEqualTo(courseNameCn);
        return certificateinfoMapper.countByExample(example);
    }

    @Override
    public Long getLearnerNumberByCourseNameCn(String courseNameCn, String state)
    {
        CertificateinfoExample example = new CertificateinfoExample();
        example.or().andCourseNameCnEqualTo(courseNameCn).andValidateEqualTo(state);
        return certificateinfoMapper.countByExample(example);
    }

    @Override
    public CourseView getCourseInfoByName(String courseNameCn)
    {
        CourseViewExample example = new CourseViewExample();
        example.or().andCourseNameCnEqualTo(courseNameCn);
        List<CourseView> courseViewList = courseViewMapper.selectByExample(example);
        if(courseViewList.size()>0)
        {
            return courseViewList.get(0);
        }
        return null;
    }


}
