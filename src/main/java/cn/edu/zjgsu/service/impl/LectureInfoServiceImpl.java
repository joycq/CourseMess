package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.LectureinfoMapper;
import cn.edu.zjgsu.model.Lectureinfo;
import cn.edu.zjgsu.model.LectureinfoExample;
import cn.edu.zjgsu.service.LectureInfoService;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.CheckUtil;
import cn.edu.zjgsu.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureInfoServiceImpl implements LectureInfoService
{
    @Autowired
    private LectureinfoMapper lectureinfoMapper;

    private LectureinfoExample lectureinfoExample;

    private ResultBean resultBean;

    private ResultPageBean resultPageBean;

    @Override
    public ResultPageBean<Lectureinfo> getLectureInfosByPaging(Long authorId, Integer pageCount, Integer pageSize)
    {
        resultPageBean = new ResultPageBean<Lectureinfo>();
        lectureinfoExample = new LectureinfoExample();

        if (authorId == null)
        {
            resultPageBean.noAuthorId();
            return resultPageBean;
        }
        if (CheckUtil.checkPage(pageCount, pageSize))
        {
            lectureinfoExample.setOffset(CommonUtil.getOffset(pageCount,pageSize));
            lectureinfoExample.setLimit(pageSize);
            lectureinfoExample.or().andAuthorIdEqualTo(authorId);
            resultPageBean.setData(lectureinfoMapper.selectByExample(lectureinfoExample));
            resultPageBean.setTotalCount((int) lectureinfoMapper.countByExample(lectureinfoExample));
            return resultPageBean;
        }
        resultPageBean.illegalPageCountOrPageSize();
        return resultPageBean;
    }

    @Override
    public ResultBean<Integer> countOfLectureInfo(Long authorId)
    {
        resultBean = new ResultBean<Integer>();
        lectureinfoExample = new LectureinfoExample();

        if (authorId==null)
        {
            resultBean.noAuthorId();
            return resultBean;
        }
        lectureinfoExample.or().andAuthorIdEqualTo(authorId);
        resultBean.setData(lectureinfoMapper.countByExample(lectureinfoExample));
        return resultBean;
    }

    @Override
    public ResultBean<Integer> getLectureInfoNumberOfPeople(List<Integer> rangeList)
    {
        resultBean = new ResultBean<Integer>();
        Integer min = rangeList.get(0);
        Integer max = rangeList.get(1);
        int resultCount;
        if(min!=null&&max!=null)
        {
            resultCount = lectureinfoMapper.getCountOfNumberSeeLecture(min,max);
            resultBean.setData(resultCount);
            return resultBean;
        }else
        {
            resultBean.fail();
            return resultBean;
        }
    }
}
