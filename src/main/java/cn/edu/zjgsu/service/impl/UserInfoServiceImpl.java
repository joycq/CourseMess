package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.*;
import cn.edu.zjgsu.exception.WrongAuthorIdException;
import cn.edu.zjgsu.model.*;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.CheckUtil;
import cn.edu.zjgsu.util.CommonUtil;
import cn.edu.zjgsu.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wqmei on 2017/10/2.
 */
@Service
//@CacheConfig(cacheNames = "userInfoCache")
public class UserInfoServiceImpl implements UserInfoService
{
    @Autowired
    private UserinfoMapper userinfoMapper;

    @Autowired
    private UserCourseViewMapper userCourseViewMapper;

    @Autowired
    private CertificateinfoMapper certificateinfoMapper;

    @Autowired
    LectureinfoMapper lectureinfoMapper;
    @Autowired
    UserStudyCourseViewMapper userStudyCourseViewMapper;

    @Autowired
    TopicinfoMapper topicinfoMapper;

    private UserinfoExample userinfoExample;

    private UserCourseViewExample userCourseViewExample;

    private CertificateinfoExample certificateinfoExample;

    private ResultBean resultBean;

    private ResultPageBean resultPageBean;

    @Override
    public Userinfo getUserInfoByAuthorId(Long authorId)
    {
        userinfoExample = new UserinfoExample();

        userinfoExample.or().andAuthorIdEqualTo(authorId);
        List<Userinfo> userinfos = userinfoMapper.selectByExample(userinfoExample);
        if (userinfos.size() == 1)
        {
            return userinfos.get(0);
        }
        return null;
    }

    @Override
    @Cacheable(cacheNames = "userInfoCache")
    public ResultPageBean<UserCourseView> getUserFollowCourseByPaging(Long authorId, Integer pageCount, Integer pageSize)
    {
        userCourseViewExample = new UserCourseViewExample();
        userinfoExample = new UserinfoExample();
        resultPageBean = new ResultPageBean<UserCourseView>();

        if (authorId == null)
        {
            resultPageBean.noAuthorId();
            return resultPageBean;
        }
        long time1 = System.currentTimeMillis();
        System.out.println("============");
        System.out.println(authorId);
        userinfoExample.or().andAuthorIdEqualTo(authorId);
        String authorName = userinfoMapper.selectByExample(userinfoExample).get(0).getAuthorName();
        if (CheckUtil.checkPage(pageCount, pageSize))
        {
            userCourseViewExample.setOffset(CommonUtil.getOffset(pageCount, pageSize));
            userCourseViewExample.setLimit(pageSize);
            userCourseViewExample.or().andAuthorNameEqualTo(authorName);
            List<UserCourseView> userCourseViews = userCourseViewMapper.selectByExample(userCourseViewExample);
            resultPageBean.setData(userCourseViews);
            resultPageBean.setTotalCount((int) userCourseViewMapper.countByExample(userCourseViewExample));
            System.out.println("====== runtime  ======");
            System.out.println(System.currentTimeMillis() - time1);
            return resultPageBean;
        }
        resultPageBean.illegalPageCountOrPageSize();
        System.out.println("======  runtime  ======");
        System.out.println(System.currentTimeMillis() - time1);
        return resultPageBean;
    }

    @Override
    public ResultPageBean<Certificateinfo> getCertificateInfoByPaging(Long authorId, Integer isValidate, Integer pageCount, Integer pageSize)
    {
        resultPageBean = new ResultPageBean<Certificateinfo>();
        certificateinfoExample = new CertificateinfoExample();

        if (authorId == null)
        {
            resultPageBean.noAuthorId();
            return resultPageBean;
        }
        if (CheckUtil.checkPage(pageCount, pageSize))
        {
            certificateinfoExample.setOffset(CommonUtil.getOffset(pageCount, pageSize));
            certificateinfoExample.setLimit(pageSize);
            if (isValidate != null)
            {
                switch (isValidate)
                {
                    case (1):
                        certificateinfoExample.or().andAuthorIdEqualTo(authorId).andValidateEqualTo("已验证");
                        break;
                    case (0):
                        certificateinfoExample.or().andAuthorIdEqualTo(authorId).andValidateEqualTo("未验证");
                        break;
                }
            }else
            {
                certificateinfoExample.or().andAuthorIdEqualTo(authorId);
            }
            resultPageBean.setData(certificateinfoMapper.selectByExample(certificateinfoExample));
            resultPageBean.setTotalCount((int) certificateinfoMapper.countByExample(certificateinfoExample));
            return resultPageBean;
        }
        resultPageBean.illegalPageCountOrPageSize();
        return resultPageBean;
    }

    @Override
    public ResultBean<Integer> getUserFollowCourseCount(Long authorId)
    {
        resultBean = new ResultBean<Integer>();
        userinfoExample = new UserinfoExample();

        if (authorId == null)
        {
            resultBean.noAuthorId();
            return resultBean;
        }
        userinfoExample.or().andAuthorIdEqualTo(authorId);
        String authorName = userinfoMapper.selectByExample(userinfoExample).get(0).getAuthorName();
        userCourseViewExample.or().andAuthorNameEqualTo(authorName);
        resultBean.setData(userCourseViewMapper.countByExample(userCourseViewExample));
        return resultBean;
    }

    @Override
    public ResultBean<Integer> getCertificateInfoCount(Long authorId, Integer isValidate)
    {
        resultBean = new ResultBean<Integer>();
        certificateinfoExample = new CertificateinfoExample();

        if (authorId == null)
        {
            resultBean.noAuthorId();
            return resultBean;
        }

        if (isValidate != null)
        {
            switch (isValidate)
            {
                case (1):
                    certificateinfoExample.or().andValidateEqualTo("已验证").andAuthorIdEqualTo(authorId);
                    break;
                case (0):
                    certificateinfoExample.or().andValidateEqualTo("未验证").andAuthorIdEqualTo(authorId);
                    break;
                default:
            }
        }else
            {
                certificateinfoExample.or().andAuthorIdEqualTo(authorId);
        }
        resultBean.setData(certificateinfoMapper.countByExample(certificateinfoExample));
        return resultBean;
    }

    @Override
    public ResultBean<Map<String, Integer>> getUserEducateNumber()
    {
        resultBean = new ResultBean<Map<String,Integer>>();
        UserinfoExample example = new UserinfoExample();

        Map<String, Integer> map = new HashMap<>();
        example.or().andEducationLike("[本科]"+"%");
        int benke = (int) userinfoMapper.countByExample(example);
        map.put("本科", benke);

        example = new UserinfoExample();
        example.or().andEducationLike("[硕士]"+"%");
        int suoshi = (int) userinfoMapper.countByExample(example);
        map.put("硕士", suoshi);

        example = new UserinfoExample();
        example.or().andEducationLike("[博士]"+"%");
        int boshi = (int) userinfoMapper.countByExample(example);
        map.put("博士", boshi);

        example = new UserinfoExample();
        example.or().andEducationLike("[专科]"+"%");
        int zhuanke = (int) userinfoMapper.countByExample(example);
        map.put("专科", zhuanke);

        example = new UserinfoExample();
        example.or().andEducationLike("[中学]"+"%");
        int zhongxue = (int) userinfoMapper.countByExample(example);
        map.put("中学", zhongxue);


        example = new UserinfoExample();
        int other = (int) userinfoMapper.countByExample(example);
        map.put("其他", (other-benke-suoshi-boshi-zhuanke-zhongxue));
        resultBean.setData(map);
        return resultBean;
    }

    @Override
    public ResultBean<Map<String, Integer>> getDifferentClassNumber()
    {

        return null;
    }

    @Override
    public Long getLectureCount(Long authorId)
    {
        if (authorId == null)
        {
            throw new WrongAuthorIdException();
        }
        LectureinfoExample lectureinfoExample = new LectureinfoExample();
        lectureinfoExample.or().andAuthorIdEqualTo(authorId);
        return (Long) lectureinfoMapper.countByExample(lectureinfoExample);
    }

    @Override
    public Page<Lectureinfo> getLectureByPaging(Long authorId, int pageCount, int pageSize)
    {
        if (authorId == null)
        {
            throw new WrongAuthorIdException();
        }
        LectureinfoExample lectureinfoExample = new LectureinfoExample();
        lectureinfoExample.or().andAuthorIdEqualTo(authorId);
        lectureinfoExample.setOffset(CommonUtil.getOffset(pageCount,pageSize));
        lectureinfoExample.setLimit(pageSize);
        List<Lectureinfo> lectureinfoList = lectureinfoMapper.selectByExample(lectureinfoExample);
        int resultCount = (int) lectureinfoMapper.countByExample(lectureinfoExample);
        Page<Lectureinfo> Page =new Page<>(lectureinfoList, (long) resultCount);
        return Page;
    }

    @Override
    public Page<UserStudyCourseView> getUserStudyCourseViewByPaging(String authorName, int pageCount, int pageSize)
    {
        if (StringUtil.isEmpty(authorName))
        {
            throw new RuntimeException();
        }
        UserStudyCourseViewExample example = new UserStudyCourseViewExample();
        example.or().andAuthorNameEqualTo(authorName);
        example.setOffset(CommonUtil.getOffset(pageCount,pageSize));
        example.setLimit(pageSize);
        Page<UserStudyCourseView> page =new Page<>();
        List<UserStudyCourseView> userStudyCourseViews = userStudyCourseViewMapper.selectByExample(example);
        Long count = userStudyCourseViewMapper.countByExample(example);
        //page.setTotalCount(userStudyCourseViewMapper.countByExample(example));
        //page.setData(userStudyCourseViewMapper.selectByExample(example));
        page.setData(userStudyCourseViews);
        page.setTotalCount(count);
        return page;
    }

    @Override
    public Page<Topicinfo> getUserTopicByPaging(Long authorId, int pageCount, int pageSize)
    {
        if (authorId == null)
        {
            throw new WrongAuthorIdException();
        }
        Page<Topicinfo> page = new Page<>();
        TopicinfoExample example = new TopicinfoExample();
        example.or().andAuthorIdEqualTo(authorId);
        //page.setTotalCount();
        return null;
    }
}
