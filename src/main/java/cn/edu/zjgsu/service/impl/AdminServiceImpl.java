package cn.edu.zjgsu.service.impl;

import cn.edu.zjgsu.dao.LectureinfoMapper;
import cn.edu.zjgsu.dao.TopicinfoMapper;
import cn.edu.zjgsu.dao.UserinfoMapper;
import cn.edu.zjgsu.dao.UserstudycourseMapper;
import cn.edu.zjgsu.model.*;
import cn.edu.zjgsu.service.AdminService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 9:30
 * Description:
 */
@Service
public class AdminServiceImpl implements AdminService
{
    @Autowired
    UserinfoMapper userinfoMapper;
    @Autowired
    UserstudycourseMapper userstudycourseMapper;
    @Autowired
    LectureinfoMapper lectureinfoMapper;
    @Autowired
    TopicinfoMapper topicinfoMapper;

    @Override
    public Page<Userinfo> getUserInfoByPaging(int pageCount, int pageSize)
    {
        UserinfoExample userinfoExample = new UserinfoExample();
        userinfoExample.setOffset(CommonUtil.getOffset(pageCount,pageSize));
        userinfoExample.setLimit(pageSize);
        Page<Userinfo> page = new Page<>();
        page.setData(userinfoMapper.selectByExample(userinfoExample));
        page.setTotalCount(userinfoMapper.countByExample(userinfoExample));
        return page;
    }

    @Override
    public Long getUserCourseNumber(String authorName)
    {
        UserstudycourseExample example = new UserstudycourseExample();
        example.or().andAuthorNameEqualTo(authorName);
        return userstudycourseMapper.countByExample(example);
    }

    @Override
    public Long getUserLectureNumber(Long authorId)
    {
        LectureinfoExample example = new LectureinfoExample();
        example.or().andAuthorIdEqualTo(authorId);
        return lectureinfoMapper.countByExample(example);
    }

    @Override
    public Long getUserTopicNumber(Long authorId)
    {
        TopicinfoExample example = new TopicinfoExample();
        example.or().andAuthorIdEqualTo(authorId);
        return topicinfoMapper.countByExample(example);
    }

}
