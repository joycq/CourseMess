package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.CourseView;
import cn.edu.zjgsu.model.Courseinfo;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;

import java.util.Map;

public interface CourseInfoService
{
    public Page<CourseView> getCourseInfoByPaging(String keyword, Integer pageCount, Integer pageSize);

    ResultBean<Map<String,Integer>> getDifferentClassNumber();

    Long getLearnerNumberByCourseId(Integer courseId);

    Long getLearnerNumberByCourseUrl(String courseUrl);

    Long getLearnerNumberByCourseNameCn(String courseNameCn);

    Long getLearnerNumberByCourseNameCn(String courseNameCn,String state);

    CourseView getCourseInfoByName(String courseNameCn);
}
