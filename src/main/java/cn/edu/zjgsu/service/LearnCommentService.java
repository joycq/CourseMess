package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.Learncomment;
import cn.edu.zjgsu.templates.ResultPageBean;

public interface LearnCommentService
{
    public ResultPageBean<Learncomment> getLearnCommentByCourseIdPaging(Integer courseId,Integer pageCount,Integer pageSize);
}
