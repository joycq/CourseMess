package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.Userinfo;
import cn.edu.zjgsu.templates.Page;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 9:30
 * Description:
 */
public interface AdminService
{

    /**
     * @param pageCount
     * @param pageSize
     * @return
     */
    Page<Userinfo> getUserInfoByPaging(int pageCount, int pageSize);

    Long getUserCourseNumber(String authorName);

    Long getUserLectureNumber(Long authorId);

    Long getUserTopicNumber(Long authorId);
}
