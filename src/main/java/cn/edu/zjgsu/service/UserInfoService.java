package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.*;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;

import java.util.Map;

/**
 * Created by wqmei on 2017/10/2.
 */
public interface UserInfoService
{
    Userinfo getUserInfoByAuthorId(Long authorId);

    ResultPageBean<UserCourseView> getUserFollowCourseByPaging(Long authorId, Integer pageCount, Integer pageSize);

    ResultPageBean<Certificateinfo> getCertificateInfoByPaging(Long authorId, Integer isValidate, Integer pageCount, Integer pageSize);

    ResultBean<Integer> getUserFollowCourseCount(Long authorId);

    ResultBean<Integer> getCertificateInfoCount(Long authorId, Integer isValidate);

    ResultBean<Map<String,Integer>> getUserEducateNumber();

    ResultBean<Map<String,Integer>> getDifferentClassNumber();

    Long getLectureCount(Long authorId);

    Page<Lectureinfo> getLectureByPaging(Long authorId, int pageCount, int pageSize);

    Page<UserStudyCourseView> getUserStudyCourseViewByPaging(String authorName, int pageCount, int pageSize);

    Page<Topicinfo> getUserTopicByPaging(Long authorId, int pageCount, int pageSize);
}
