package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.Author;

/**
 * Created by wqmei on 2017/10/2.
 */
public interface AuthorService
{
    Boolean login(Author author);

    Long getAuthorId(Author author);

    String getRoleByLoginName(String loginName);
}
