package cn.edu.zjgsu.service;

import cn.edu.zjgsu.model.Lectureinfo;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;

import java.util.List;

public interface LectureInfoService
{
    ResultPageBean<Lectureinfo> getLectureInfosByPaging(Long authorId,Integer pageCount,Integer pageSize);

    ResultBean<Integer> countOfLectureInfo(Long authorId);

    ResultBean<Integer> getLectureInfoNumberOfPeople(List<Integer> list1);
}
