package cn.edu.zjgsu.exception;

/**
 * @author: wqmei
 * DateTime: 2017/10/27 20:05
 * Description:
 */
public class WrongAuthorIdException extends RuntimeException
{
    public WrongAuthorIdException()
    {
        super("用户id不存在");
    }

    public WrongAuthorIdException(String message)
    {
        super(message);
    }
}
