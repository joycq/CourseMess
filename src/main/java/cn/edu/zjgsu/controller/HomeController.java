package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.service.CourseInfoService;
import cn.edu.zjgsu.service.LectureInfoService;
import cn.edu.zjgsu.service.TopicService;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.util.Constant;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@EnableAutoConfiguration
public class HomeController
{
    @Autowired
    private  UserInfoService userInfoService;

    @Autowired
    private CourseInfoService courseInfoService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private LectureInfoService lectureInfoService;

    @GetMapping(path = "/count/educateNumber")
    public ResultBean<Map<String, Integer>> getUserEducateNumber()
    {
        return userInfoService.getUserEducateNumber();
    }


    @GetMapping(path = "/count/classNumber")
    public ResultBean<Map<String, Integer>> getDifferentClassNumber()
    {
        return courseInfoService.getDifferentClassNumber();
    }


    /**
     * 获取不同听过不同范围数量专题的人数(如听过10场到100场专题的用户人数)
     * @param array
     * @return
     */
    @PostMapping(path = "/count/topicNumberOfPeople")
    public ResultBean<List<Integer>> getTopicNumberOfPeopleSee(@RequestBody JSONObject array)
    {
        ResultBean<List<Integer>> result = new ResultBean<List<Integer>>();
        List<Integer> resultList = new ArrayList<Integer>();

        ResultBean<Integer> temp;
        List<List<Integer>> arrays = (List<List<Integer>>) array.get("array");
        for (List<Integer> list1 : arrays)
        {
            if(list1.size()!=2)
            {
                System.out.println("=======");
                System.out.println(list1.get(0));
                System.out.println(list1.get(1));
                result.fail();
                result.setMsg("输入参数有误");
                result.setData(null);
                return result;
            }else
            {
                temp = topicService.getTopicNumberOfPeople(list1);
                if(temp.getCode()== Constant.FAIL)
                {
                    result.fail();
                    result.setMsg("查询出错");
                    result.setData(null);
                    return result;
                }else
                {
                    resultList.add(temp.getData());
                }
            }
        }
        result.setData(resultList);
        return result;
    }

    /**
     * 获取不同听过不同范围数量演讲的人数(如听过10场到100场演讲的用户人数)
     * @param array
     * @return
     */
    @PostMapping(path = "/count/lectureNumberOfPeople")
    public ResultBean<List<Integer>> getLectureNumberOfPeopleSee(@RequestBody JSONObject array)
    {
        ResultBean<List<Integer>> result = new ResultBean<List<Integer>>();
        List<Integer> resultList = new ArrayList<Integer>();

        ResultBean<Integer> temp;
        List<List<Integer>> arrays = (List<List<Integer>>) array.get("array");
        for (List<Integer> list1 : arrays)
        {
            if(list1.size()!=2)
            {
                System.out.println("=======");
                System.out.println(list1.get(0));
                System.out.println(list1.get(1));
                result.fail();
                result.setMsg("输入参数有误");
                result.setData(null);
                return result;
            }else
            {
                temp = lectureInfoService.getLectureInfoNumberOfPeople(list1);
                if(temp.getCode()== Constant.FAIL)
                {
                    result.fail();
                    result.setMsg("查询出错");
                    result.setData(null);
                    return result;
                }else
                {
                    resultList.add(temp.getData());
                }
            }
        }
        result.setData(resultList);
        return result;
    }
}
