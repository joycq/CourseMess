package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.exception.WrongAuthorIdException;
import cn.edu.zjgsu.model.Certificateinfo;
import cn.edu.zjgsu.model.Lectureinfo;
import cn.edu.zjgsu.model.UserCourseView;
import cn.edu.zjgsu.model.Userinfo;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@EnableAutoConfiguration
public class UserInfoController
{
    @Autowired
    private UserInfoService userInfoService;


    /**
     * 获取用户信息
     *
     * @param request
     * @return
     */
    @GetMapping("/author/userInfo")
    public ResultBean<Userinfo> getUserInfo(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        ResultBean<Userinfo> resultBean = new ResultBean();
        if (session.getAttribute(Constant.AUTHOR_ID) == null)
        {
            resultBean.setCode(ResultBean.FAIL);
            return resultBean;
        }
        resultBean.setData(userInfoService.getUserInfoByAuthorId((long) session.getAttribute(Constant.AUTHOR_ID)));
        return resultBean;
    }

    /**
     * 获取用户关注课程
     *
     * @param pageCount
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping(path = "/author/followCourse/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<UserCourseView> getUserFollowCourseByPaging(@PathVariable Integer pageCount, @PathVariable Integer pageSize, HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        Long authorId = (Long) session.getAttribute(Constant.AUTHOR_ID);
        return userInfoService.getUserFollowCourseByPaging(authorId, pageCount, pageSize);
    }

    /**
     * 获取用户关注课程数量
     *
     * @return
     */
    @GetMapping(path = "/author/count/followCourse")
    public ResultBean<Integer> getUseFollowCourseCount(HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute(Constant.AUTHOR_ID);
        return userInfoService.getUserFollowCourseCount(authorId);
    }

    /**
     * 获取用户验证课程信息
     *
     * @param pageCount
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping(path = "/author/certificateInfo/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Certificateinfo> getCertificateInfoByPaging(@PathVariable Integer pageCount,
                                                                      @PathVariable Integer pageSize, @RequestParam(required = false) Integer isVilidate,
                                                                      HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute(Constant.AUTHOR_ID);
        return userInfoService.getCertificateInfoByPaging(authorId, isVilidate, pageCount, pageSize);
    }

    /**
     * 获取用户验证课程数量
     *
     * @param isValidate
     * @param request
     * @return
     */
    @GetMapping(path = "/author/count/certificateInfo")
    public ResultBean<Integer> getCerticateInfoCount(@RequestParam(required = false) Integer isValidate, HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute(Constant.AUTHOR_ID);
        return userInfoService.getCertificateInfoCount(authorId, isValidate);
    }

    /**
     * 获得用户听过的演讲数量
     * @param request
     * @return
     */
    @GetMapping(path = "/author/count/lecture")
    public ResultBean<Long> getLectureCount(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        Long authorId = (Long) session.getAttribute(Constant.AUTHOR_ID);
        Long count = null;
        ResultBean<Long> resultBean = new ResultBean<>();
        try
        {
            count = userInfoService.getLectureCount(authorId);
        } catch (WrongAuthorIdException e)
        {
            resultBean.fail();
            resultBean.setMsg("找不到用户id");
            return resultBean;
        }
        resultBean.setData(count);
        return resultBean;
    }

    /**
     * 得到当前用户关注的演讲数
     * @param pageCount
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping(path = "/author/lecture/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Lectureinfo> getLectureInfo(@PathVariable int pageCount, @PathVariable int pageSize, HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        Long authorId = (Long) session.getAttribute(Constant.AUTHOR_ID);
        Page<Lectureinfo> page = null;
        ResultPageBean<Lectureinfo> resultPageBean = new ResultPageBean<>();
        try
        {
            page = userInfoService.getLectureByPaging(authorId, pageCount, pageSize);
        } catch (WrongAuthorIdException e)
        {
            e.printStackTrace();
            resultPageBean.fail();
            resultPageBean.setMsg("找不到用户id");
        }
        resultPageBean.setData(page.getData());
        resultPageBean.setTotalCount(page.getTotalCount());
        return resultPageBean;

    }
}
