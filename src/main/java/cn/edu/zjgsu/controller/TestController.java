package cn.edu.zjgsu.controller;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: wqmei
 * Date: 2017/10/18 14:32
 * Description:测试类
 */
@RestController
public class TestController
{
    @Cacheable(value = "testCache")
    @GetMapping(path = "hello")
    public String hello(@RequestParam(required = false) String s1)
    {
        System.out.println("==================");
        System.out.println("recieve "+s1==null?"":s1);
        return "Hello World";
    }
}
