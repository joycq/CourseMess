package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.model.Lectureinfo;
import cn.edu.zjgsu.service.LectureInfoService;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@EnableAutoConfiguration
public class LectureController
{
    @Autowired
    private LectureInfoService lectureInfoService;

    /**
     *用户看过的演讲
     * @param pageCount
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping("/author/lectureInfo/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Lectureinfo> getLectureInfosByPaging(@PathVariable Integer pageCount,
                                                               @PathVariable Integer pageSize, HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute("authorId");
        return lectureInfoService.getLectureInfosByPaging(authorId, pageCount, pageSize);
    }

    @GetMapping(path = "/author/count/lectureInfo")
    public ResultBean<Integer> getLectureInfoCount(HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute("authorId");
        return lectureInfoService.countOfLectureInfo(authorId);

    }

}
