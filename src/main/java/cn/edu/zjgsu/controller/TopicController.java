package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.model.Topicinfo;
import cn.edu.zjgsu.service.TopicService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.Constant;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class TopicController
{
    @Autowired
    private TopicService topicService;

    /**
     * 获取用户看过的专题
     * @param pageCount
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping("/author/topicInfo/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Topicinfo> getTopicInfosByPaging(@PathVariable Integer pageCount,
                                                           @PathVariable Integer pageSize, HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute(Constant.AUTHOR_ID);
        Page<Topicinfo> page = topicService.getTopicsByPaging(authorId, pageCount, pageSize);
        ResultPageBean<Topicinfo> result = new ResultPageBean<>();
        result.setData(page.getData());
        result.setTotalCount(page.getTotalCount());
        return result;
    }

    /**
     * 获取用户看过的专题数量
     * @param request
     * @return
     */
    @GetMapping(path = "/author/count/topicInfo")
    public ResultBean<Integer> getTopicInfoCount(HttpServletRequest request)
    {
        Long authorId = (Long) request.getSession().getAttribute(Constant.AUTHOR_ID);
        return topicService.getTopicCount(authorId);
    }

}
