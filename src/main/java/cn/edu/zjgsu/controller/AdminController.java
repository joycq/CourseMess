package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.model.*;
import cn.edu.zjgsu.service.AdminService;
import cn.edu.zjgsu.service.TopicService;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.vo.UserInfoOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: wqmei
 * DateTime: 2017/10/28 9:12
 * Description:
 */
@RestController
@RequestMapping(value = "admin")
public class AdminController
{
    @Autowired
    AdminService adminService;
    @Autowired
    UserInfoService userInfoService;
    @Autowired
    TopicService topicService;

    /**
     * 分页获取用户信息
     * @param pageCount
     * @param pageSize
     * @return
     */
    @GetMapping("/userInfo/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<UserInfoOutput> getUserInfoByPaging(@PathVariable int pageCount,@PathVariable int pageSize)
    {
        Page<Userinfo> userinfoPage = adminService.getUserInfoByPaging(pageCount,pageSize);
        List<Userinfo> userinfos = userinfoPage.getData();
        Long totalCount = userinfoPage.getTotalCount();
        ResultPageBean<UserInfoOutput> result = new ResultPageBean<>();
        result.setTotalCount(totalCount);

        List<UserInfoOutput> userInfoOutputList = new ArrayList<>();
        UserInfoOutput userInfoOutput = null;
        Long authorId ;
        String authorName;
        for (int i = 0,length=userinfos.size(); i <length ; i++)
        {
            userInfoOutput = new UserInfoOutput();
            authorId = userinfos.get(i).getAuthorId();
            authorName = userinfos.get(i).getAuthorName();
            userInfoOutput.setAuthorId(authorId);
            userInfoOutput.setAuthorName(authorName);
            userInfoOutput.setCourseNumber(adminService.getUserCourseNumber(authorName));
            userInfoOutput.setLectureNumber(adminService.getUserLectureNumber(authorId));
            userInfoOutput.setTopicNumber(adminService.getUserTopicNumber(authorId));
            userInfoOutputList.add(userInfoOutput);
        }
        result.setData(userInfoOutputList);
        return result;
    }


    /**
     * 获取用户学习课程信息
     * @param pageCount
     * @param pageSize
     * @param authorName
     * @return
     */
    @GetMapping("/userStudyCourse/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<UserStudyCourseView> getUserStudyCourseDetialByPaging(@PathVariable int pageCount,
                                                                                @PathVariable int pageSize, @RequestParam String authorName)
    {
        ResultPageBean<UserStudyCourseView> result = new ResultPageBean<>();
        Page<UserStudyCourseView> page = userInfoService.getUserStudyCourseViewByPaging(authorName,pageCount,pageSize);
        result.setTotalCount(page.getTotalCount());
        result.setData(page.getData());
        return result;
    }

    /**
     * 获取用户观看的演讲详情
     * @param pageCount
     * @param pageSize
     * @param authorId
     * @return
     */
    @GetMapping("/userLecture/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Lectureinfo> getUserLectureInfoByPaging(@PathVariable int pageCount,
                                                                  @PathVariable int pageSize,@RequestParam Long authorId)
    {
        ResultPageBean<Lectureinfo> result = new ResultPageBean<>();
        Page<Lectureinfo> page = userInfoService.getLectureByPaging(authorId, pageCount, pageSize);
        result.setTotalCount(page.getTotalCount());
        result.setData(page.getData());
        return result;
    }

    /**
     * 获取用户关注的专题
     * @param pageCount
     * @param pageSize
     * @param authorId
     * @return
     */
    @GetMapping("/userTopic/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Topicinfo> getUserTopicInfoByPaging(@PathVariable int pageCount,
                                                              @PathVariable int pageSize,@RequestParam Long authorId)
    {
        ResultPageBean<Topicinfo> result = new ResultPageBean<>();
        Page<Topicinfo> page = topicService.getTopicsByPaging(authorId, pageCount, pageSize);
        result.setTotalCount(page.getTotalCount());
        result.setData(page.getData());
        return result;
    }


}
