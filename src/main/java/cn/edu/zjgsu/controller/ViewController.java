package cn.edu.zjgsu.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
@EnableAutoConfiguration
public class ViewController
{
    @GetMapping("/page/{path}")
    public String index(@PathVariable String path)
    {
        System.out.println(path);
        return path;
    }

    @GetMapping(path = "test/login")
    public String test()
    {
        return "login";
    }
}
