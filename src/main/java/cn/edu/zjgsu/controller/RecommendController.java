package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.model.CourseView;
import cn.edu.zjgsu.model.PopularCourse;
import cn.edu.zjgsu.model.Recommend;
import cn.edu.zjgsu.model.Userinfo;
import cn.edu.zjgsu.service.CourseInfoService;
import cn.edu.zjgsu.service.RecommendService;
import cn.edu.zjgsu.service.UserInfoService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.RequestUtil;
import cn.edu.zjgsu.vo.CourseOutPut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: wqmei
 * DateTime: 2017/11/23 16:39
 * Description:
 */
@RestController
@RequestMapping(path = "/api/recommend")
public class RecommendController
{
    @Autowired
    UserInfoService userInfoService;
    @Autowired
    RecommendService recommendService;
    @Autowired
    CourseInfoService courseInfoService;

    /**
     * 查询用户推荐课程
     * @param request
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @GetMapping("")
    public ResultPageBean<CourseOutPut> getRecommendCourse(HttpServletRequest request,
                                                           @RequestParam(required = false)Integer pageNumber,
                                                           @RequestParam(required = false)Integer pageSize)
    {
        ResultPageBean<CourseOutPut> result = new ResultPageBean();
        //数据过滤
        if (pageNumber == null)
        {
            pageNumber = 1;
        }
        //if(pageSize==null||pageSize>2)
        if(pageSize==null)
        {
            pageSize = 2;
        }
        //获取authorId
        Long authorId = RequestUtil.getAuthorId(request);
        //获取用户名称
        Userinfo userinfo = userInfoService.getUserInfoByAuthorId(authorId);
        String authorName = userinfo.getAuthorName();
        //获取内容
        Page<Recommend> recommendPage = recommendService.getRecommendByAuthorName(authorName,pageNumber,pageSize);

        //新方法
        List<String> recommendCoursesNew = recommendService.getRecommendCourseNameByAuthorName(authorName,pageSize);
        List<Recommend> recommendList = recommendPage.getData();

        List<CourseOutPut> courseOutPutList = new ArrayList<>();
        /*CourseView courseView = null;
        CourseOutPut courseOutPut = null;
        //当查询到记录说明有推荐课程,根据名称循环查询具体记录
        for (int i = 0,length=recommendList.size(); i <recommendList.size() ; i++)
        {
            courseView = courseInfoService.getCourseInfoByName(recommendList.get(i).getCourseNameCn());
            courseOutPut = new CourseOutPut().reconvert(courseView);
            courseOutPutList.add(courseOutPut);
        }

        //如果数量不足,即推荐课程只有1门,获取2门,则去最受欢迎
        int listSize = recommendList.size();
        if(listSize<pageNumber)
        {
            int popularNumber = pageNumber - listSize;
            Page<PopularCourse> popularCoursePage = recommendService.getPopularCoursePage(1,popularNumber);
            List<PopularCourse> popularCourseList = popularCoursePage.getData();
            for (int i = 0,length = popularCourseList.size(); i < length; i++)
            {
                courseView = courseInfoService.getCourseInfoByName(popularCourseList.get(i).getCourseNameCn());
                courseOutPut = new CourseOutPut().reconvert(courseView);
                courseOutPutList.add(courseOutPut);
            }
        }*/
        courseOutPutList = getRecommendByCourseName(recommendCoursesNew,pageSize);
        result.setData(courseOutPutList);
        return result;
    }

    public List<CourseOutPut> getRecommendByCourseName(List<String> recommendCoursesNew,int pageSize)
    {
        //int pageSize = recommendCoursesNew.size();
        List<CourseOutPut> courseOutPutList = new ArrayList<>();
        CourseView courseView = null;
        CourseOutPut courseOutPut = null;
        //当查询到记录说明有推荐课程,根据名称循环查询具体记录
        for (int i = 0,length=recommendCoursesNew.size(); i <recommendCoursesNew.size() ; i++)
        {
            courseView = courseInfoService.getCourseInfoByName(recommendCoursesNew.get(i));
            courseOutPut = new CourseOutPut().reconvert(courseView);
            courseOutPutList.add(courseOutPut);
        }

        //如果数量不足,即推荐课程只有1门,获取2门,则去最受欢迎
        int listSize = recommendCoursesNew.size();
        if(listSize< pageSize)
        {
            int popularNumber = pageSize - listSize;
            Page<PopularCourse> popularCoursePage = recommendService.getPopularCoursePage(1,popularNumber);
            List<PopularCourse> popularCourseList = popularCoursePage.getData();
            for (int i = 0,length = popularCourseList.size(); i < length; i++)
            {
                courseView = courseInfoService.getCourseInfoByName(popularCourseList.get(i).getCourseNameCn());
                courseOutPut = new CourseOutPut().reconvert(courseView);
                courseOutPutList.add(courseOutPut);
            }
        }
        return courseOutPutList;
    }
}
