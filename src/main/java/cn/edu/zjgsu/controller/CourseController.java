package cn.edu.zjgsu.controller;

import cn.edu.zjgsu.model.CourseView;
import cn.edu.zjgsu.model.Courseinfo;
import cn.edu.zjgsu.model.Learncomment;
import cn.edu.zjgsu.service.CourseInfoService;
import cn.edu.zjgsu.service.LearnCommentService;
import cn.edu.zjgsu.templates.Page;
import cn.edu.zjgsu.templates.ResultBean;
import cn.edu.zjgsu.templates.ResultPageBean;
import cn.edu.zjgsu.util.Constant;
import cn.edu.zjgsu.vo.CourseLearnOutput;
import cn.edu.zjgsu.vo.CourseOutPut;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@EnableAutoConfiguration
public class CourseController
{
    @Autowired
    private CourseInfoService courseInfoService;

    @Autowired
    private LearnCommentService learnCommentService;

    /**
     * 获取课程信息
     * @param pageCount
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping(path = "/course/courseInfo/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<CourseOutPut> getUserInfoByPaging(@PathVariable Integer pageCount, @PathVariable Integer pageSize,
                                                          @RequestParam(required = false) String keyword)
    {

        ResultPageBean<CourseOutPut> result = new ResultPageBean<>();

        Page<CourseView> courseViewPage = courseInfoService.getCourseInfoByPaging(keyword, pageCount, pageSize);
        List<CourseView> courseViewList = courseViewPage.getData();
        List<CourseOutPut> courseOutPutList = new ArrayList<>();
        CourseOutPut courseOutPut = null;
        String courseNamecn;
        for (int i=0,length=courseViewList.size();i<length;i++)
        {
            courseOutPut = new CourseOutPut();
            courseOutPut.reconvert(courseViewList.get(i));

            courseNamecn = courseOutPut.getCourseNameCn();
            courseOutPut.setTotalCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNamecn));
            courseOutPut.setValidateCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNamecn,Constant.VALIDATE_STR));
            courseOutPut.setNotValidateCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNamecn,Constant.NOT_VALIDATE_STR));

            courseOutPutList.add(courseOutPut);
        }

        result.setData(courseOutPutList);
        result.setTotalCount(courseViewPage.getTotalCount());
        return result;
    }



    /**
     * 获取课程评价
     * @param courseId
     * @param pageCount
     * @param pageSize
     * @return
     */
    @GetMapping(path = "/course/comment/pageCount/{pageCount}/pageSize/{pageSize}")
    public ResultPageBean<Learncomment> getLearnCommentByCourseIdPaging(@RequestParam Integer courseId,
                                                                        @PathVariable Integer pageCount,@PathVariable Integer pageSize)
    {
        return learnCommentService.getLearnCommentByCourseIdPaging(courseId, pageCount,pageSize);
    }

    /**
     * 获取课程通过信息
     * @param jsonObject
     * @return
     */
    @PostMapping(path = "/course/count/learner")
    public ResultBean<CourseLearnOutput> getCourseLearnerNumberByCourseId(@RequestBody JSONObject jsonObject)
    {
        ResultBean<CourseLearnOutput> result = new ResultBean<>();
        //result.setData(courseInfoService.getLearnerNumberByCourseId(courseId));
        //result.setData(courseInfoService.getLearnerNumberByCourseUrl(courseUrl));
        String courseNameCn = jsonObject.getString("courseNameCn");
        CourseLearnOutput courseLearnOutput = CourseLearnOutput.builder()
                .totalCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNameCn))
                .validateCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNameCn, Constant.VALIDATE_STR))
                .notValidateCount(courseInfoService.getLearnerNumberByCourseNameCn(courseNameCn, Constant.NOT_VALIDATE_STR)).build();

        result.setData(courseLearnOutput);
        return result;
    }

}
