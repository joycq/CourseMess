package cn.edu.zjgsu.util;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author: wqmei
 * DateTime: 2017/11/23 16:43
 * Description:获取request中的参数
 */
@Slf4j
public final class RequestUtil
{
    public static final Long getAuthorId(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        Object object = session.getAttribute(Constant.AUTHOR_ID);
        if (object == null)
        {
            log.error("获取authorId时不存在,session出错");
            throw new RuntimeException();
        }
        return (Long)object;
    }
}
