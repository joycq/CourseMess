package cn.edu.zjgsu.util;

public final class CheckUtil
{
    public static Boolean checkPage(Integer pageCount, Integer PageSize)
    {
        if (pageCount != null && PageSize != null && pageCount > 0 && PageSize > 0)
        {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
