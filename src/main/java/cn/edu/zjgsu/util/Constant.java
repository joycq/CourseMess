package cn.edu.zjgsu.util;

public final class Constant
{
    public static final String ENCRYP_SALT = "CNEDUZJGSU";

    public static final Integer FAIL = 0;

    public static final int SUCCESS = 1;

    public static final int NO_PERMISSION = 2;

    /**
     * 登录后在session中存放authorId,这样前端传值不需要传用户id,可以直接查询当前用户的数据
     */
    public static final String AUTHOR_ID = "authorId";

    public static final String VALIDATE_STR = "已验证";

    public static final String NOT_VALIDATE_STR = "未验证";

}
