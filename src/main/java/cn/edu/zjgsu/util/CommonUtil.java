package cn.edu.zjgsu.util;

public final class CommonUtil
{
    public static final int getOffset(int pageCount,int pageSize)
    {
        return Math.max(0, (pageCount - 1) * pageSize);
    }
}
