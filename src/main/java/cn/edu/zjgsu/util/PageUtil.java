package cn.edu.zjgsu.util;

/**
 * @author: wqmei
 * DateTime: 2017/11/23 16:43
 * Description:
 */
public class PageUtil
{
    public static final int getOffset(Integer pageNumber,Integer pageSize)
    {
        return Math.max(0, (pageNumber - 1) * pageSize);
    }

    public static final boolean validPage(Integer pageNumber,Integer pageSize)
    {
        return pageNumber != null && pageSize != null;
    }
}
