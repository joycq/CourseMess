package cn.edu.zjgsu.model;

import java.util.ArrayList;
import java.util.List;

public class FollowerinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public FollowerinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAuthorIdIsNull() {
            addCriterion("author_id is null");
            return (Criteria) this;
        }

        public Criteria andAuthorIdIsNotNull() {
            addCriterion("author_id is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorIdEqualTo(Long value) {
            addCriterion("author_id =", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotEqualTo(Long value) {
            addCriterion("author_id <>", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdGreaterThan(Long value) {
            addCriterion("author_id >", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdGreaterThanOrEqualTo(Long value) {
            addCriterion("author_id >=", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdLessThan(Long value) {
            addCriterion("author_id <", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdLessThanOrEqualTo(Long value) {
            addCriterion("author_id <=", value, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdIn(List<Long> values) {
            addCriterion("author_id in", values, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotIn(List<Long> values) {
            addCriterion("author_id not in", values, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdBetween(Long value1, Long value2) {
            addCriterion("author_id between", value1, value2, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorIdNotBetween(Long value1, Long value2) {
            addCriterion("author_id not between", value1, value2, "authorId");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlIsNull() {
            addCriterion("author_url is null");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlIsNotNull() {
            addCriterion("author_url is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlEqualTo(String value) {
            addCriterion("author_url =", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlNotEqualTo(String value) {
            addCriterion("author_url <>", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlGreaterThan(String value) {
            addCriterion("author_url >", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlGreaterThanOrEqualTo(String value) {
            addCriterion("author_url >=", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlLessThan(String value) {
            addCriterion("author_url <", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlLessThanOrEqualTo(String value) {
            addCriterion("author_url <=", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlLike(String value) {
            addCriterion("author_url like", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlNotLike(String value) {
            addCriterion("author_url not like", value, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlIn(List<String> values) {
            addCriterion("author_url in", values, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlNotIn(List<String> values) {
            addCriterion("author_url not in", values, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlBetween(String value1, String value2) {
            addCriterion("author_url between", value1, value2, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorUrlNotBetween(String value1, String value2) {
            addCriterion("author_url not between", value1, value2, "authorUrl");
            return (Criteria) this;
        }

        public Criteria andAuthorNameIsNull() {
            addCriterion("author_name is null");
            return (Criteria) this;
        }

        public Criteria andAuthorNameIsNotNull() {
            addCriterion("author_name is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorNameEqualTo(String value) {
            addCriterion("author_name =", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameNotEqualTo(String value) {
            addCriterion("author_name <>", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameGreaterThan(String value) {
            addCriterion("author_name >", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameGreaterThanOrEqualTo(String value) {
            addCriterion("author_name >=", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameLessThan(String value) {
            addCriterion("author_name <", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameLessThanOrEqualTo(String value) {
            addCriterion("author_name <=", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameLike(String value) {
            addCriterion("author_name like", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameNotLike(String value) {
            addCriterion("author_name not like", value, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameIn(List<String> values) {
            addCriterion("author_name in", values, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameNotIn(List<String> values) {
            addCriterion("author_name not in", values, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameBetween(String value1, String value2) {
            addCriterion("author_name between", value1, value2, "authorName");
            return (Criteria) this;
        }

        public Criteria andAuthorNameNotBetween(String value1, String value2) {
            addCriterion("author_name not between", value1, value2, "authorName");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlIsNull() {
            addCriterion("follower_url is null");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlIsNotNull() {
            addCriterion("follower_url is not null");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlEqualTo(String value) {
            addCriterion("follower_url =", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlNotEqualTo(String value) {
            addCriterion("follower_url <>", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlGreaterThan(String value) {
            addCriterion("follower_url >", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlGreaterThanOrEqualTo(String value) {
            addCriterion("follower_url >=", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlLessThan(String value) {
            addCriterion("follower_url <", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlLessThanOrEqualTo(String value) {
            addCriterion("follower_url <=", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlLike(String value) {
            addCriterion("follower_url like", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlNotLike(String value) {
            addCriterion("follower_url not like", value, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlIn(List<String> values) {
            addCriterion("follower_url in", values, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlNotIn(List<String> values) {
            addCriterion("follower_url not in", values, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlBetween(String value1, String value2) {
            addCriterion("follower_url between", value1, value2, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerUrlNotBetween(String value1, String value2) {
            addCriterion("follower_url not between", value1, value2, "followerUrl");
            return (Criteria) this;
        }

        public Criteria andFollowerNameIsNull() {
            addCriterion("follower_name is null");
            return (Criteria) this;
        }

        public Criteria andFollowerNameIsNotNull() {
            addCriterion("follower_name is not null");
            return (Criteria) this;
        }

        public Criteria andFollowerNameEqualTo(String value) {
            addCriterion("follower_name =", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameNotEqualTo(String value) {
            addCriterion("follower_name <>", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameGreaterThan(String value) {
            addCriterion("follower_name >", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameGreaterThanOrEqualTo(String value) {
            addCriterion("follower_name >=", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameLessThan(String value) {
            addCriterion("follower_name <", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameLessThanOrEqualTo(String value) {
            addCriterion("follower_name <=", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameLike(String value) {
            addCriterion("follower_name like", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameNotLike(String value) {
            addCriterion("follower_name not like", value, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameIn(List<String> values) {
            addCriterion("follower_name in", values, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameNotIn(List<String> values) {
            addCriterion("follower_name not in", values, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameBetween(String value1, String value2) {
            addCriterion("follower_name between", value1, value2, "followerName");
            return (Criteria) this;
        }

        public Criteria andFollowerNameNotBetween(String value1, String value2) {
            addCriterion("follower_name not between", value1, value2, "followerName");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}