package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class Followerinfo implements Serializable {
    private Integer id;

    private Long authorId;

    private String authorUrl;

    private String authorName;

    private String followerUrl;

    private String followerName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getFollowerUrl() {
        return followerUrl;
    }

    public void setFollowerUrl(String followerUrl) {
        this.followerUrl = followerUrl;
    }

    public String getFollowerName() {
        return followerName;
    }

    public void setFollowerName(String followerName) {
        this.followerName = followerName;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Followerinfo other = (Followerinfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAuthorId() == null ? other.getAuthorId() == null : this.getAuthorId().equals(other.getAuthorId()))
            && (this.getAuthorUrl() == null ? other.getAuthorUrl() == null : this.getAuthorUrl().equals(other.getAuthorUrl()))
            && (this.getAuthorName() == null ? other.getAuthorName() == null : this.getAuthorName().equals(other.getAuthorName()))
            && (this.getFollowerUrl() == null ? other.getFollowerUrl() == null : this.getFollowerUrl().equals(other.getFollowerUrl()))
            && (this.getFollowerName() == null ? other.getFollowerName() == null : this.getFollowerName().equals(other.getFollowerName()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAuthorId() == null) ? 0 : getAuthorId().hashCode());
        result = prime * result + ((getAuthorUrl() == null) ? 0 : getAuthorUrl().hashCode());
        result = prime * result + ((getAuthorName() == null) ? 0 : getAuthorName().hashCode());
        result = prime * result + ((getFollowerUrl() == null) ? 0 : getFollowerUrl().hashCode());
        result = prime * result + ((getFollowerName() == null) ? 0 : getFollowerName().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", authorId=").append(authorId);
        sb.append(", authorUrl=").append(authorUrl);
        sb.append(", authorName=").append(authorName);
        sb.append(", followerUrl=").append(followerUrl);
        sb.append(", followerName=").append(followerName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}