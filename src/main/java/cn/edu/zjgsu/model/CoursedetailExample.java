package cn.edu.zjgsu.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class CoursedetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Integer offset;

    public CoursedetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andClassNameIsNull() {
            addCriterion("class_name is null");
            return (Criteria) this;
        }

        public Criteria andClassNameIsNotNull() {
            addCriterion("class_name is not null");
            return (Criteria) this;
        }

        public Criteria andClassNameEqualTo(String value) {
            addCriterion("class_name =", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotEqualTo(String value) {
            addCriterion("class_name <>", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameGreaterThan(String value) {
            addCriterion("class_name >", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameGreaterThanOrEqualTo(String value) {
            addCriterion("class_name >=", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLessThan(String value) {
            addCriterion("class_name <", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLessThanOrEqualTo(String value) {
            addCriterion("class_name <=", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameLike(String value) {
            addCriterion("class_name like", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotLike(String value) {
            addCriterion("class_name not like", value, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameIn(List<String> values) {
            addCriterion("class_name in", values, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotIn(List<String> values) {
            addCriterion("class_name not in", values, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameBetween(String value1, String value2) {
            addCriterion("class_name between", value1, value2, "className");
            return (Criteria) this;
        }

        public Criteria andClassNameNotBetween(String value1, String value2) {
            addCriterion("class_name not between", value1, value2, "className");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNull() {
            addCriterion("course_id is null");
            return (Criteria) this;
        }

        public Criteria andCourseIdIsNotNull() {
            addCriterion("course_id is not null");
            return (Criteria) this;
        }

        public Criteria andCourseIdEqualTo(Integer value) {
            addCriterion("course_id =", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotEqualTo(Integer value) {
            addCriterion("course_id <>", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThan(Integer value) {
            addCriterion("course_id >", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("course_id >=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThan(Integer value) {
            addCriterion("course_id <", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdLessThanOrEqualTo(Integer value) {
            addCriterion("course_id <=", value, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdIn(List<Integer> values) {
            addCriterion("course_id in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotIn(List<Integer> values) {
            addCriterion("course_id not in", values, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdBetween(Integer value1, Integer value2) {
            addCriterion("course_id between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("course_id not between", value1, value2, "courseId");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnIsNull() {
            addCriterion("course_name_cn is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnIsNotNull() {
            addCriterion("course_name_cn is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnEqualTo(String value) {
            addCriterion("course_name_cn =", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnNotEqualTo(String value) {
            addCriterion("course_name_cn <>", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnGreaterThan(String value) {
            addCriterion("course_name_cn >", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnGreaterThanOrEqualTo(String value) {
            addCriterion("course_name_cn >=", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnLessThan(String value) {
            addCriterion("course_name_cn <", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnLessThanOrEqualTo(String value) {
            addCriterion("course_name_cn <=", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnLike(String value) {
            addCriterion("course_name_cn like", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnNotLike(String value) {
            addCriterion("course_name_cn not like", value, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnIn(List<String> values) {
            addCriterion("course_name_cn in", values, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnNotIn(List<String> values) {
            addCriterion("course_name_cn not in", values, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnBetween(String value1, String value2) {
            addCriterion("course_name_cn between", value1, value2, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameCnNotBetween(String value1, String value2) {
            addCriterion("course_name_cn not between", value1, value2, "courseNameCn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnIsNull() {
            addCriterion("course_name_en is null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnIsNotNull() {
            addCriterion("course_name_en is not null");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnEqualTo(String value) {
            addCriterion("course_name_en =", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnNotEqualTo(String value) {
            addCriterion("course_name_en <>", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnGreaterThan(String value) {
            addCriterion("course_name_en >", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnGreaterThanOrEqualTo(String value) {
            addCriterion("course_name_en >=", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnLessThan(String value) {
            addCriterion("course_name_en <", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnLessThanOrEqualTo(String value) {
            addCriterion("course_name_en <=", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnLike(String value) {
            addCriterion("course_name_en like", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnNotLike(String value) {
            addCriterion("course_name_en not like", value, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnIn(List<String> values) {
            addCriterion("course_name_en in", values, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnNotIn(List<String> values) {
            addCriterion("course_name_en not in", values, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnBetween(String value1, String value2) {
            addCriterion("course_name_en between", value1, value2, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseNameEnNotBetween(String value1, String value2) {
            addCriterion("course_name_en not between", value1, value2, "courseNameEn");
            return (Criteria) this;
        }

        public Criteria andCourseUrlIsNull() {
            addCriterion("course_url is null");
            return (Criteria) this;
        }

        public Criteria andCourseUrlIsNotNull() {
            addCriterion("course_url is not null");
            return (Criteria) this;
        }

        public Criteria andCourseUrlEqualTo(String value) {
            addCriterion("course_url =", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlNotEqualTo(String value) {
            addCriterion("course_url <>", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlGreaterThan(String value) {
            addCriterion("course_url >", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlGreaterThanOrEqualTo(String value) {
            addCriterion("course_url >=", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlLessThan(String value) {
            addCriterion("course_url <", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlLessThanOrEqualTo(String value) {
            addCriterion("course_url <=", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlLike(String value) {
            addCriterion("course_url like", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlNotLike(String value) {
            addCriterion("course_url not like", value, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlIn(List<String> values) {
            addCriterion("course_url in", values, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlNotIn(List<String> values) {
            addCriterion("course_url not in", values, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlBetween(String value1, String value2) {
            addCriterion("course_url between", value1, value2, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andCourseUrlNotBetween(String value1, String value2) {
            addCriterion("course_url not between", value1, value2, "courseUrl");
            return (Criteria) this;
        }

        public Criteria andStarIsNull() {
            addCriterion("star is null");
            return (Criteria) this;
        }

        public Criteria andStarIsNotNull() {
            addCriterion("star is not null");
            return (Criteria) this;
        }

        public Criteria andStarEqualTo(String value) {
            addCriterion("star =", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarNotEqualTo(String value) {
            addCriterion("star <>", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarGreaterThan(String value) {
            addCriterion("star >", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarGreaterThanOrEqualTo(String value) {
            addCriterion("star >=", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarLessThan(String value) {
            addCriterion("star <", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarLessThanOrEqualTo(String value) {
            addCriterion("star <=", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarLike(String value) {
            addCriterion("star like", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarNotLike(String value) {
            addCriterion("star not like", value, "star");
            return (Criteria) this;
        }

        public Criteria andStarIn(List<String> values) {
            addCriterion("star in", values, "star");
            return (Criteria) this;
        }

        public Criteria andStarNotIn(List<String> values) {
            addCriterion("star not in", values, "star");
            return (Criteria) this;
        }

        public Criteria andStarBetween(String value1, String value2) {
            addCriterion("star between", value1, value2, "star");
            return (Criteria) this;
        }

        public Criteria andStarNotBetween(String value1, String value2) {
            addCriterion("star not between", value1, value2, "star");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(String value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(String value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(String value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(String value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(String value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(String value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLike(String value) {
            addCriterion("score like", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotLike(String value) {
            addCriterion("score not like", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<String> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<String> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(String value1, String value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(String value1, String value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andPersonFollowIsNull() {
            addCriterion("person_follow is null");
            return (Criteria) this;
        }

        public Criteria andPersonFollowIsNotNull() {
            addCriterion("person_follow is not null");
            return (Criteria) this;
        }

        public Criteria andPersonFollowEqualTo(String value) {
            addCriterion("person_follow =", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowNotEqualTo(String value) {
            addCriterion("person_follow <>", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowGreaterThan(String value) {
            addCriterion("person_follow >", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowGreaterThanOrEqualTo(String value) {
            addCriterion("person_follow >=", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowLessThan(String value) {
            addCriterion("person_follow <", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowLessThanOrEqualTo(String value) {
            addCriterion("person_follow <=", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowLike(String value) {
            addCriterion("person_follow like", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowNotLike(String value) {
            addCriterion("person_follow not like", value, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowIn(List<String> values) {
            addCriterion("person_follow in", values, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowNotIn(List<String> values) {
            addCriterion("person_follow not in", values, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowBetween(String value1, String value2) {
            addCriterion("person_follow between", value1, value2, "personFollow");
            return (Criteria) this;
        }

        public Criteria andPersonFollowNotBetween(String value1, String value2) {
            addCriterion("person_follow not between", value1, value2, "personFollow");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(String value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(String value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(String value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(String value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(String value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLike(String value) {
            addCriterion("start_time like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotLike(String value) {
            addCriterion("start_time not like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<String> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<String> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(String value1, String value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(String value1, String value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeIsNull() {
            addCriterion("duration_time is null");
            return (Criteria) this;
        }

        public Criteria andDurationTimeIsNotNull() {
            addCriterion("duration_time is not null");
            return (Criteria) this;
        }

        public Criteria andDurationTimeEqualTo(String value) {
            addCriterion("duration_time =", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeNotEqualTo(String value) {
            addCriterion("duration_time <>", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeGreaterThan(String value) {
            addCriterion("duration_time >", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeGreaterThanOrEqualTo(String value) {
            addCriterion("duration_time >=", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeLessThan(String value) {
            addCriterion("duration_time <", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeLessThanOrEqualTo(String value) {
            addCriterion("duration_time <=", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeLike(String value) {
            addCriterion("duration_time like", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeNotLike(String value) {
            addCriterion("duration_time not like", value, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeIn(List<String> values) {
            addCriterion("duration_time in", values, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeNotIn(List<String> values) {
            addCriterion("duration_time not in", values, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeBetween(String value1, String value2) {
            addCriterion("duration_time between", value1, value2, "durationTime");
            return (Criteria) this;
        }

        public Criteria andDurationTimeNotBetween(String value1, String value2) {
            addCriterion("duration_time not between", value1, value2, "durationTime");
            return (Criteria) this;
        }

        public Criteria andCourseLevelIsNull() {
            addCriterion("course_level is null");
            return (Criteria) this;
        }

        public Criteria andCourseLevelIsNotNull() {
            addCriterion("course_level is not null");
            return (Criteria) this;
        }

        public Criteria andCourseLevelEqualTo(String value) {
            addCriterion("course_level =", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelNotEqualTo(String value) {
            addCriterion("course_level <>", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelGreaterThan(String value) {
            addCriterion("course_level >", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelGreaterThanOrEqualTo(String value) {
            addCriterion("course_level >=", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelLessThan(String value) {
            addCriterion("course_level <", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelLessThanOrEqualTo(String value) {
            addCriterion("course_level <=", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelLike(String value) {
            addCriterion("course_level like", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelNotLike(String value) {
            addCriterion("course_level not like", value, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelIn(List<String> values) {
            addCriterion("course_level in", values, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelNotIn(List<String> values) {
            addCriterion("course_level not in", values, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelBetween(String value1, String value2) {
            addCriterion("course_level between", value1, value2, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andCourseLevelNotBetween(String value1, String value2) {
            addCriterion("course_level not between", value1, value2, "courseLevel");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVIsNull() {
            addCriterion("knowledge_V is null");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVIsNotNull() {
            addCriterion("knowledge_V is not null");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVEqualTo(String value) {
            addCriterion("knowledge_V =", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVNotEqualTo(String value) {
            addCriterion("knowledge_V <>", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVGreaterThan(String value) {
            addCriterion("knowledge_V >", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVGreaterThanOrEqualTo(String value) {
            addCriterion("knowledge_V >=", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVLessThan(String value) {
            addCriterion("knowledge_V <", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVLessThanOrEqualTo(String value) {
            addCriterion("knowledge_V <=", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVLike(String value) {
            addCriterion("knowledge_V like", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVNotLike(String value) {
            addCriterion("knowledge_V not like", value, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVIn(List<String> values) {
            addCriterion("knowledge_V in", values, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVNotIn(List<String> values) {
            addCriterion("knowledge_V not in", values, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVBetween(String value1, String value2) {
            addCriterion("knowledge_V between", value1, value2, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andKnowledgeVNotBetween(String value1, String value2) {
            addCriterion("knowledge_V not between", value1, value2, "knowledgeV");
            return (Criteria) this;
        }

        public Criteria andTeacherVIsNull() {
            addCriterion("teacher_V is null");
            return (Criteria) this;
        }

        public Criteria andTeacherVIsNotNull() {
            addCriterion("teacher_V is not null");
            return (Criteria) this;
        }

        public Criteria andTeacherVEqualTo(String value) {
            addCriterion("teacher_V =", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVNotEqualTo(String value) {
            addCriterion("teacher_V <>", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVGreaterThan(String value) {
            addCriterion("teacher_V >", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVGreaterThanOrEqualTo(String value) {
            addCriterion("teacher_V >=", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVLessThan(String value) {
            addCriterion("teacher_V <", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVLessThanOrEqualTo(String value) {
            addCriterion("teacher_V <=", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVLike(String value) {
            addCriterion("teacher_V like", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVNotLike(String value) {
            addCriterion("teacher_V not like", value, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVIn(List<String> values) {
            addCriterion("teacher_V in", values, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVNotIn(List<String> values) {
            addCriterion("teacher_V not in", values, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVBetween(String value1, String value2) {
            addCriterion("teacher_V between", value1, value2, "teacherV");
            return (Criteria) this;
        }

        public Criteria andTeacherVNotBetween(String value1, String value2) {
            addCriterion("teacher_V not between", value1, value2, "teacherV");
            return (Criteria) this;
        }

        public Criteria andInterestVIsNull() {
            addCriterion("interest_V is null");
            return (Criteria) this;
        }

        public Criteria andInterestVIsNotNull() {
            addCriterion("interest_V is not null");
            return (Criteria) this;
        }

        public Criteria andInterestVEqualTo(String value) {
            addCriterion("interest_V =", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVNotEqualTo(String value) {
            addCriterion("interest_V <>", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVGreaterThan(String value) {
            addCriterion("interest_V >", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVGreaterThanOrEqualTo(String value) {
            addCriterion("interest_V >=", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVLessThan(String value) {
            addCriterion("interest_V <", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVLessThanOrEqualTo(String value) {
            addCriterion("interest_V <=", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVLike(String value) {
            addCriterion("interest_V like", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVNotLike(String value) {
            addCriterion("interest_V not like", value, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVIn(List<String> values) {
            addCriterion("interest_V in", values, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVNotIn(List<String> values) {
            addCriterion("interest_V not in", values, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVBetween(String value1, String value2) {
            addCriterion("interest_V between", value1, value2, "interestV");
            return (Criteria) this;
        }

        public Criteria andInterestVNotBetween(String value1, String value2) {
            addCriterion("interest_V not between", value1, value2, "interestV");
            return (Criteria) this;
        }

        public Criteria andDesignVIsNull() {
            addCriterion("design_V is null");
            return (Criteria) this;
        }

        public Criteria andDesignVIsNotNull() {
            addCriterion("design_V is not null");
            return (Criteria) this;
        }

        public Criteria andDesignVEqualTo(String value) {
            addCriterion("design_V =", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVNotEqualTo(String value) {
            addCriterion("design_V <>", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVGreaterThan(String value) {
            addCriterion("design_V >", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVGreaterThanOrEqualTo(String value) {
            addCriterion("design_V >=", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVLessThan(String value) {
            addCriterion("design_V <", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVLessThanOrEqualTo(String value) {
            addCriterion("design_V <=", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVLike(String value) {
            addCriterion("design_V like", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVNotLike(String value) {
            addCriterion("design_V not like", value, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVIn(List<String> values) {
            addCriterion("design_V in", values, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVNotIn(List<String> values) {
            addCriterion("design_V not in", values, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVBetween(String value1, String value2) {
            addCriterion("design_V between", value1, value2, "designV");
            return (Criteria) this;
        }

        public Criteria andDesignVNotBetween(String value1, String value2) {
            addCriterion("design_V not between", value1, value2, "designV");
            return (Criteria) this;
        }

        public Criteria andCommentTagIsNull() {
            addCriterion("comment_tag is null");
            return (Criteria) this;
        }

        public Criteria andCommentTagIsNotNull() {
            addCriterion("comment_tag is not null");
            return (Criteria) this;
        }

        public Criteria andCommentTagEqualTo(String value) {
            addCriterion("comment_tag =", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagNotEqualTo(String value) {
            addCriterion("comment_tag <>", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagGreaterThan(String value) {
            addCriterion("comment_tag >", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagGreaterThanOrEqualTo(String value) {
            addCriterion("comment_tag >=", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagLessThan(String value) {
            addCriterion("comment_tag <", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagLessThanOrEqualTo(String value) {
            addCriterion("comment_tag <=", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagLike(String value) {
            addCriterion("comment_tag like", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagNotLike(String value) {
            addCriterion("comment_tag not like", value, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagIn(List<String> values) {
            addCriterion("comment_tag in", values, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagNotIn(List<String> values) {
            addCriterion("comment_tag not in", values, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagBetween(String value1, String value2) {
            addCriterion("comment_tag between", value1, value2, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCommentTagNotBetween(String value1, String value2) {
            addCriterion("comment_tag not between", value1, value2, "commentTag");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleIsNull() {
            addCriterion("course_score_people is null");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleIsNotNull() {
            addCriterion("course_score_people is not null");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleEqualTo(String value) {
            addCriterion("course_score_people =", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleNotEqualTo(String value) {
            addCriterion("course_score_people <>", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleGreaterThan(String value) {
            addCriterion("course_score_people >", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleGreaterThanOrEqualTo(String value) {
            addCriterion("course_score_people >=", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleLessThan(String value) {
            addCriterion("course_score_people <", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleLessThanOrEqualTo(String value) {
            addCriterion("course_score_people <=", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleLike(String value) {
            addCriterion("course_score_people like", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleNotLike(String value) {
            addCriterion("course_score_people not like", value, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleIn(List<String> values) {
            addCriterion("course_score_people in", values, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleNotIn(List<String> values) {
            addCriterion("course_score_people not in", values, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleBetween(String value1, String value2) {
            addCriterion("course_score_people between", value1, value2, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andCourseScorePeopleNotBetween(String value1, String value2) {
            addCriterion("course_score_people not between", value1, value2, "courseScorePeople");
            return (Criteria) this;
        }

        public Criteria andRemarkNumIsNull() {
            addCriterion("remark_num is null");
            return (Criteria) this;
        }

        public Criteria andRemarkNumIsNotNull() {
            addCriterion("remark_num is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkNumEqualTo(Integer value) {
            addCriterion("remark_num =", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumNotEqualTo(Integer value) {
            addCriterion("remark_num <>", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumGreaterThan(Integer value) {
            addCriterion("remark_num >", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("remark_num >=", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumLessThan(Integer value) {
            addCriterion("remark_num <", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumLessThanOrEqualTo(Integer value) {
            addCriterion("remark_num <=", value, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumIn(List<Integer> values) {
            addCriterion("remark_num in", values, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumNotIn(List<Integer> values) {
            addCriterion("remark_num not in", values, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumBetween(Integer value1, Integer value2) {
            addCriterion("remark_num between", value1, value2, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andRemarkNumNotBetween(Integer value1, Integer value2) {
            addCriterion("remark_num not between", value1, value2, "remarkNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumIsNull() {
            addCriterion("note_num is null");
            return (Criteria) this;
        }

        public Criteria andNoteNumIsNotNull() {
            addCriterion("note_num is not null");
            return (Criteria) this;
        }

        public Criteria andNoteNumEqualTo(Integer value) {
            addCriterion("note_num =", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumNotEqualTo(Integer value) {
            addCriterion("note_num <>", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumGreaterThan(Integer value) {
            addCriterion("note_num >", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("note_num >=", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumLessThan(Integer value) {
            addCriterion("note_num <", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumLessThanOrEqualTo(Integer value) {
            addCriterion("note_num <=", value, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumIn(List<Integer> values) {
            addCriterion("note_num in", values, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumNotIn(List<Integer> values) {
            addCriterion("note_num not in", values, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumBetween(Integer value1, Integer value2) {
            addCriterion("note_num between", value1, value2, "noteNum");
            return (Criteria) this;
        }

        public Criteria andNoteNumNotBetween(Integer value1, Integer value2) {
            addCriterion("note_num not between", value1, value2, "noteNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumIsNull() {
            addCriterion("talk_num is null");
            return (Criteria) this;
        }

        public Criteria andTalkNumIsNotNull() {
            addCriterion("talk_num is not null");
            return (Criteria) this;
        }

        public Criteria andTalkNumEqualTo(Integer value) {
            addCriterion("talk_num =", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumNotEqualTo(Integer value) {
            addCriterion("talk_num <>", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumGreaterThan(Integer value) {
            addCriterion("talk_num >", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("talk_num >=", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumLessThan(Integer value) {
            addCriterion("talk_num <", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumLessThanOrEqualTo(Integer value) {
            addCriterion("talk_num <=", value, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumIn(List<Integer> values) {
            addCriterion("talk_num in", values, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumNotIn(List<Integer> values) {
            addCriterion("talk_num not in", values, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumBetween(Integer value1, Integer value2) {
            addCriterion("talk_num between", value1, value2, "talkNum");
            return (Criteria) this;
        }

        public Criteria andTalkNumNotBetween(Integer value1, Integer value2) {
            addCriterion("talk_num not between", value1, value2, "talkNum");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}