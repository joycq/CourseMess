package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class Recommend implements Serializable {
    private Long recommmend;

    private String authorName;

    private String courseNameCn;

    private Integer courseId;

    private Integer course2;

    private Integer weight;

    private static final long serialVersionUID = 1L;

    public Long getRecommmend() {
        return recommmend;
    }

    public void setRecommmend(Long recommmend) {
        this.recommmend = recommmend;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getCourseNameCn() {
        return courseNameCn;
    }

    public void setCourseNameCn(String courseNameCn) {
        this.courseNameCn = courseNameCn;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getCourse2() {
        return course2;
    }

    public void setCourse2(Integer course2) {
        this.course2 = course2;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Recommend other = (Recommend) that;
        return (this.getRecommmend() == null ? other.getRecommmend() == null : this.getRecommmend().equals(other.getRecommmend()))
            && (this.getAuthorName() == null ? other.getAuthorName() == null : this.getAuthorName().equals(other.getAuthorName()))
            && (this.getCourseNameCn() == null ? other.getCourseNameCn() == null : this.getCourseNameCn().equals(other.getCourseNameCn()))
            && (this.getCourseId() == null ? other.getCourseId() == null : this.getCourseId().equals(other.getCourseId()))
            && (this.getCourse2() == null ? other.getCourse2() == null : this.getCourse2().equals(other.getCourse2()))
            && (this.getWeight() == null ? other.getWeight() == null : this.getWeight().equals(other.getWeight()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRecommmend() == null) ? 0 : getRecommmend().hashCode());
        result = prime * result + ((getAuthorName() == null) ? 0 : getAuthorName().hashCode());
        result = prime * result + ((getCourseNameCn() == null) ? 0 : getCourseNameCn().hashCode());
        result = prime * result + ((getCourseId() == null) ? 0 : getCourseId().hashCode());
        result = prime * result + ((getCourse2() == null) ? 0 : getCourse2().hashCode());
        result = prime * result + ((getWeight() == null) ? 0 : getWeight().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", recommmend=").append(recommmend);
        sb.append(", authorName=").append(authorName);
        sb.append(", courseNameCn=").append(courseNameCn);
        sb.append(", courseId=").append(courseId);
        sb.append(", course2=").append(course2);
        sb.append(", weight=").append(weight);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}