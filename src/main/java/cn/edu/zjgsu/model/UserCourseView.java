package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class UserCourseView implements Serializable {
    private String authorName;

    private String courseNameCn;

    private String courseNameEn;

    private String courseUrl;

    private String star;

    private String className;

    private Integer courseId;

    private String score;

    private String personFollow;

    private String startTime;

    private String durationTime;

    private String courseLevel;

    private String knowledgeV;

    private String teacherV;

    private String interestV;

    private String designV;

    private String commentTag;

    private String courseScorePeople;

    private Integer remarkNum;

    private Integer noteNum;

    private Integer talkNum;

    private static final long serialVersionUID = 1L;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getCourseNameCn() {
        return courseNameCn;
    }

    public void setCourseNameCn(String courseNameCn) {
        this.courseNameCn = courseNameCn;
    }

    public String getCourseNameEn() {
        return courseNameEn;
    }

    public void setCourseNameEn(String courseNameEn) {
        this.courseNameEn = courseNameEn;
    }

    public String getCourseUrl() {
        return courseUrl;
    }

    public void setCourseUrl(String courseUrl) {
        this.courseUrl = courseUrl;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getPersonFollow() {
        return personFollow;
    }

    public void setPersonFollow(String personFollow) {
        this.personFollow = personFollow;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(String courseLevel) {
        this.courseLevel = courseLevel;
    }

    public String getKnowledgeV() {
        return knowledgeV;
    }

    public void setKnowledgeV(String knowledgeV) {
        this.knowledgeV = knowledgeV;
    }

    public String getTeacherV() {
        return teacherV;
    }

    public void setTeacherV(String teacherV) {
        this.teacherV = teacherV;
    }

    public String getInterestV() {
        return interestV;
    }

    public void setInterestV(String interestV) {
        this.interestV = interestV;
    }

    public String getDesignV() {
        return designV;
    }

    public void setDesignV(String designV) {
        this.designV = designV;
    }

    public String getCommentTag() {
        return commentTag;
    }

    public void setCommentTag(String commentTag) {
        this.commentTag = commentTag;
    }

    public String getCourseScorePeople() {
        return courseScorePeople;
    }

    public void setCourseScorePeople(String courseScorePeople) {
        this.courseScorePeople = courseScorePeople;
    }

    public Integer getRemarkNum() {
        return remarkNum;
    }

    public void setRemarkNum(Integer remarkNum) {
        this.remarkNum = remarkNum;
    }

    public Integer getNoteNum() {
        return noteNum;
    }

    public void setNoteNum(Integer noteNum) {
        this.noteNum = noteNum;
    }

    public Integer getTalkNum() {
        return talkNum;
    }

    public void setTalkNum(Integer talkNum) {
        this.talkNum = talkNum;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserCourseView other = (UserCourseView) that;
        return (this.getAuthorName() == null ? other.getAuthorName() == null : this.getAuthorName().equals(other.getAuthorName()))
            && (this.getCourseNameCn() == null ? other.getCourseNameCn() == null : this.getCourseNameCn().equals(other.getCourseNameCn()))
            && (this.getCourseNameEn() == null ? other.getCourseNameEn() == null : this.getCourseNameEn().equals(other.getCourseNameEn()))
            && (this.getCourseUrl() == null ? other.getCourseUrl() == null : this.getCourseUrl().equals(other.getCourseUrl()))
            && (this.getStar() == null ? other.getStar() == null : this.getStar().equals(other.getStar()))
            && (this.getClassName() == null ? other.getClassName() == null : this.getClassName().equals(other.getClassName()))
            && (this.getCourseId() == null ? other.getCourseId() == null : this.getCourseId().equals(other.getCourseId()))
            && (this.getScore() == null ? other.getScore() == null : this.getScore().equals(other.getScore()))
            && (this.getPersonFollow() == null ? other.getPersonFollow() == null : this.getPersonFollow().equals(other.getPersonFollow()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getDurationTime() == null ? other.getDurationTime() == null : this.getDurationTime().equals(other.getDurationTime()))
            && (this.getCourseLevel() == null ? other.getCourseLevel() == null : this.getCourseLevel().equals(other.getCourseLevel()))
            && (this.getKnowledgeV() == null ? other.getKnowledgeV() == null : this.getKnowledgeV().equals(other.getKnowledgeV()))
            && (this.getTeacherV() == null ? other.getTeacherV() == null : this.getTeacherV().equals(other.getTeacherV()))
            && (this.getInterestV() == null ? other.getInterestV() == null : this.getInterestV().equals(other.getInterestV()))
            && (this.getDesignV() == null ? other.getDesignV() == null : this.getDesignV().equals(other.getDesignV()))
            && (this.getCommentTag() == null ? other.getCommentTag() == null : this.getCommentTag().equals(other.getCommentTag()))
            && (this.getCourseScorePeople() == null ? other.getCourseScorePeople() == null : this.getCourseScorePeople().equals(other.getCourseScorePeople()))
            && (this.getRemarkNum() == null ? other.getRemarkNum() == null : this.getRemarkNum().equals(other.getRemarkNum()))
            && (this.getNoteNum() == null ? other.getNoteNum() == null : this.getNoteNum().equals(other.getNoteNum()))
            && (this.getTalkNum() == null ? other.getTalkNum() == null : this.getTalkNum().equals(other.getTalkNum()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAuthorName() == null) ? 0 : getAuthorName().hashCode());
        result = prime * result + ((getCourseNameCn() == null) ? 0 : getCourseNameCn().hashCode());
        result = prime * result + ((getCourseNameEn() == null) ? 0 : getCourseNameEn().hashCode());
        result = prime * result + ((getCourseUrl() == null) ? 0 : getCourseUrl().hashCode());
        result = prime * result + ((getStar() == null) ? 0 : getStar().hashCode());
        result = prime * result + ((getClassName() == null) ? 0 : getClassName().hashCode());
        result = prime * result + ((getCourseId() == null) ? 0 : getCourseId().hashCode());
        result = prime * result + ((getScore() == null) ? 0 : getScore().hashCode());
        result = prime * result + ((getPersonFollow() == null) ? 0 : getPersonFollow().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getDurationTime() == null) ? 0 : getDurationTime().hashCode());
        result = prime * result + ((getCourseLevel() == null) ? 0 : getCourseLevel().hashCode());
        result = prime * result + ((getKnowledgeV() == null) ? 0 : getKnowledgeV().hashCode());
        result = prime * result + ((getTeacherV() == null) ? 0 : getTeacherV().hashCode());
        result = prime * result + ((getInterestV() == null) ? 0 : getInterestV().hashCode());
        result = prime * result + ((getDesignV() == null) ? 0 : getDesignV().hashCode());
        result = prime * result + ((getCommentTag() == null) ? 0 : getCommentTag().hashCode());
        result = prime * result + ((getCourseScorePeople() == null) ? 0 : getCourseScorePeople().hashCode());
        result = prime * result + ((getRemarkNum() == null) ? 0 : getRemarkNum().hashCode());
        result = prime * result + ((getNoteNum() == null) ? 0 : getNoteNum().hashCode());
        result = prime * result + ((getTalkNum() == null) ? 0 : getTalkNum().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", authorName=").append(authorName);
        sb.append(", courseNameCn=").append(courseNameCn);
        sb.append(", courseNameEn=").append(courseNameEn);
        sb.append(", courseUrl=").append(courseUrl);
        sb.append(", star=").append(star);
        sb.append(", className=").append(className);
        sb.append(", courseId=").append(courseId);
        sb.append(", score=").append(score);
        sb.append(", personFollow=").append(personFollow);
        sb.append(", startTime=").append(startTime);
        sb.append(", durationTime=").append(durationTime);
        sb.append(", courseLevel=").append(courseLevel);
        sb.append(", knowledgeV=").append(knowledgeV);
        sb.append(", teacherV=").append(teacherV);
        sb.append(", interestV=").append(interestV);
        sb.append(", designV=").append(designV);
        sb.append(", commentTag=").append(commentTag);
        sb.append(", courseScorePeople=").append(courseScorePeople);
        sb.append(", remarkNum=").append(remarkNum);
        sb.append(", noteNum=").append(noteNum);
        sb.append(", talkNum=").append(talkNum);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}