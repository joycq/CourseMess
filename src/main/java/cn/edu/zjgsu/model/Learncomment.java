package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class Learncomment implements Serializable {
    private Integer id;

    private Integer courseId;

    private String courseNameCn;

    private String courseNameEn;

    private Long authorId;

    private String authorUrl;

    private String authorName;

    private Double commentStar;

    private String cmtDone;

    private String cmtTime;

    private Integer zan;

    private String cmtComment;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseNameCn() {
        return courseNameCn;
    }

    public void setCourseNameCn(String courseNameCn) {
        this.courseNameCn = courseNameCn;
    }

    public String getCourseNameEn() {
        return courseNameEn;
    }

    public void setCourseNameEn(String courseNameEn) {
        this.courseNameEn = courseNameEn;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Double getCommentStar() {
        return commentStar;
    }

    public void setCommentStar(Double commentStar) {
        this.commentStar = commentStar;
    }

    public String getCmtDone() {
        return cmtDone;
    }

    public void setCmtDone(String cmtDone) {
        this.cmtDone = cmtDone;
    }

    public String getCmtTime() {
        return cmtTime;
    }

    public void setCmtTime(String cmtTime) {
        this.cmtTime = cmtTime;
    }

    public Integer getZan() {
        return zan;
    }

    public void setZan(Integer zan) {
        this.zan = zan;
    }

    public String getCmtComment() {
        return cmtComment;
    }

    public void setCmtComment(String cmtComment) {
        this.cmtComment = cmtComment;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Learncomment other = (Learncomment) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCourseId() == null ? other.getCourseId() == null : this.getCourseId().equals(other.getCourseId()))
            && (this.getCourseNameCn() == null ? other.getCourseNameCn() == null : this.getCourseNameCn().equals(other.getCourseNameCn()))
            && (this.getCourseNameEn() == null ? other.getCourseNameEn() == null : this.getCourseNameEn().equals(other.getCourseNameEn()))
            && (this.getAuthorId() == null ? other.getAuthorId() == null : this.getAuthorId().equals(other.getAuthorId()))
            && (this.getAuthorUrl() == null ? other.getAuthorUrl() == null : this.getAuthorUrl().equals(other.getAuthorUrl()))
            && (this.getAuthorName() == null ? other.getAuthorName() == null : this.getAuthorName().equals(other.getAuthorName()))
            && (this.getCommentStar() == null ? other.getCommentStar() == null : this.getCommentStar().equals(other.getCommentStar()))
            && (this.getCmtDone() == null ? other.getCmtDone() == null : this.getCmtDone().equals(other.getCmtDone()))
            && (this.getCmtTime() == null ? other.getCmtTime() == null : this.getCmtTime().equals(other.getCmtTime()))
            && (this.getZan() == null ? other.getZan() == null : this.getZan().equals(other.getZan()))
            && (this.getCmtComment() == null ? other.getCmtComment() == null : this.getCmtComment().equals(other.getCmtComment()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCourseId() == null) ? 0 : getCourseId().hashCode());
        result = prime * result + ((getCourseNameCn() == null) ? 0 : getCourseNameCn().hashCode());
        result = prime * result + ((getCourseNameEn() == null) ? 0 : getCourseNameEn().hashCode());
        result = prime * result + ((getAuthorId() == null) ? 0 : getAuthorId().hashCode());
        result = prime * result + ((getAuthorUrl() == null) ? 0 : getAuthorUrl().hashCode());
        result = prime * result + ((getAuthorName() == null) ? 0 : getAuthorName().hashCode());
        result = prime * result + ((getCommentStar() == null) ? 0 : getCommentStar().hashCode());
        result = prime * result + ((getCmtDone() == null) ? 0 : getCmtDone().hashCode());
        result = prime * result + ((getCmtTime() == null) ? 0 : getCmtTime().hashCode());
        result = prime * result + ((getZan() == null) ? 0 : getZan().hashCode());
        result = prime * result + ((getCmtComment() == null) ? 0 : getCmtComment().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", courseId=").append(courseId);
        sb.append(", courseNameCn=").append(courseNameCn);
        sb.append(", courseNameEn=").append(courseNameEn);
        sb.append(", authorId=").append(authorId);
        sb.append(", authorUrl=").append(authorUrl);
        sb.append(", authorName=").append(authorName);
        sb.append(", commentStar=").append(commentStar);
        sb.append(", cmtDone=").append(cmtDone);
        sb.append(", cmtTime=").append(cmtTime);
        sb.append(", zan=").append(zan);
        sb.append(", cmtComment=").append(cmtComment);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}