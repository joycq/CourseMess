package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class Topicinfo implements Serializable {
    private Integer id;

    private Long authorId;

    private String authorUrl;

    private String authorName;

    private String zoneTxtTitle;

    private Long zoneTxtId;

    private String zoneTxtNum;

    private String zoneCollect;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getZoneTxtTitle() {
        return zoneTxtTitle;
    }

    public void setZoneTxtTitle(String zoneTxtTitle) {
        this.zoneTxtTitle = zoneTxtTitle;
    }

    public Long getZoneTxtId() {
        return zoneTxtId;
    }

    public void setZoneTxtId(Long zoneTxtId) {
        this.zoneTxtId = zoneTxtId;
    }

    public String getZoneTxtNum() {
        return zoneTxtNum;
    }

    public void setZoneTxtNum(String zoneTxtNum) {
        this.zoneTxtNum = zoneTxtNum;
    }

    public String getZoneCollect() {
        return zoneCollect;
    }

    public void setZoneCollect(String zoneCollect) {
        this.zoneCollect = zoneCollect;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Topicinfo other = (Topicinfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAuthorId() == null ? other.getAuthorId() == null : this.getAuthorId().equals(other.getAuthorId()))
            && (this.getAuthorUrl() == null ? other.getAuthorUrl() == null : this.getAuthorUrl().equals(other.getAuthorUrl()))
            && (this.getAuthorName() == null ? other.getAuthorName() == null : this.getAuthorName().equals(other.getAuthorName()))
            && (this.getZoneTxtTitle() == null ? other.getZoneTxtTitle() == null : this.getZoneTxtTitle().equals(other.getZoneTxtTitle()))
            && (this.getZoneTxtId() == null ? other.getZoneTxtId() == null : this.getZoneTxtId().equals(other.getZoneTxtId()))
            && (this.getZoneTxtNum() == null ? other.getZoneTxtNum() == null : this.getZoneTxtNum().equals(other.getZoneTxtNum()))
            && (this.getZoneCollect() == null ? other.getZoneCollect() == null : this.getZoneCollect().equals(other.getZoneCollect()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAuthorId() == null) ? 0 : getAuthorId().hashCode());
        result = prime * result + ((getAuthorUrl() == null) ? 0 : getAuthorUrl().hashCode());
        result = prime * result + ((getAuthorName() == null) ? 0 : getAuthorName().hashCode());
        result = prime * result + ((getZoneTxtTitle() == null) ? 0 : getZoneTxtTitle().hashCode());
        result = prime * result + ((getZoneTxtId() == null) ? 0 : getZoneTxtId().hashCode());
        result = prime * result + ((getZoneTxtNum() == null) ? 0 : getZoneTxtNum().hashCode());
        result = prime * result + ((getZoneCollect() == null) ? 0 : getZoneCollect().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", authorId=").append(authorId);
        sb.append(", authorUrl=").append(authorUrl);
        sb.append(", authorName=").append(authorName);
        sb.append(", zoneTxtTitle=").append(zoneTxtTitle);
        sb.append(", zoneTxtId=").append(zoneTxtId);
        sb.append(", zoneTxtNum=").append(zoneTxtNum);
        sb.append(", zoneCollect=").append(zoneCollect);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}