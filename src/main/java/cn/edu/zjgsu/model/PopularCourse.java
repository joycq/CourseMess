package cn.edu.zjgsu.model;

import java.io.Serializable;

/**
 * @author 
 */
public class PopularCourse implements Serializable {
    private Long popularCourseId;

    private Integer courseId;

    private String courseNameCn;

    private Integer number;

    private static final long serialVersionUID = 1L;

    public Long getPopularCourseId() {
        return popularCourseId;
    }

    public void setPopularCourseId(Long popularCourseId) {
        this.popularCourseId = popularCourseId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseNameCn() {
        return courseNameCn;
    }

    public void setCourseNameCn(String courseNameCn) {
        this.courseNameCn = courseNameCn;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PopularCourse other = (PopularCourse) that;
        return (this.getPopularCourseId() == null ? other.getPopularCourseId() == null : this.getPopularCourseId().equals(other.getPopularCourseId()))
            && (this.getCourseId() == null ? other.getCourseId() == null : this.getCourseId().equals(other.getCourseId()))
            && (this.getCourseNameCn() == null ? other.getCourseNameCn() == null : this.getCourseNameCn().equals(other.getCourseNameCn()))
            && (this.getNumber() == null ? other.getNumber() == null : this.getNumber().equals(other.getNumber()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPopularCourseId() == null) ? 0 : getPopularCourseId().hashCode());
        result = prime * result + ((getCourseId() == null) ? 0 : getCourseId().hashCode());
        result = prime * result + ((getCourseNameCn() == null) ? 0 : getCourseNameCn().hashCode());
        result = prime * result + ((getNumber() == null) ? 0 : getNumber().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", popularCourseId=").append(popularCourseId);
        sb.append(", courseId=").append(courseId);
        sb.append(", courseNameCn=").append(courseNameCn);
        sb.append(", number=").append(number);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}